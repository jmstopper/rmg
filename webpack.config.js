export default (options, argv) => {

    let filename = '[name]';

    // If we are running a production build we need to add a hash for cache
    // busting
    if (options.production === true) {
        filename = filename + '.[chunkhash]';
    }

    // Add the file extension depending on the target
    filename = filename + '.js';

    let config = {
        mode: (options.production === true ? 'production' : 'development'),
        output: {
            filename: filename
        },
        module: {
            rules: [
                // -------------------------------------------------------------
                // Linting
                // -------------------------------------------------------------
                {
                    enforce: 'pre',
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: 'eslint-loader',
                },
                // -------------------------------------------------------------
                // Build for older browsers
                // -------------------------------------------------------------
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: "babel-loader",
                }
            ]
        },
        externals: {
            jquery: 'jQuery',
            $: 'jQuery',
            jQuery: 'jQuery'
        }
    };

    return config;
};
