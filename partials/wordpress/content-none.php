<?php

ob_start();
// -----------------------------------------------------------------------------
// Start content output
// -----------------------------------------------------------------------------

?><p><?php

    _e('It appears you have found a dead end. Perhaps searching can help.', 'stratum');

?></p><?php

get_search_form();

// -----------------------------------------------------------------------------
// End content output
// -----------------------------------------------------------------------------

$content = ob_get_clean();

echo \Stratum\render('assets/components/wysiwyg', [
    'heading' => __('Sorry', 'stratum'),
    'content' => $content
]);
