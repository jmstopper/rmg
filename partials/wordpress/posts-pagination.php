<?php

the_posts_pagination([
    'prev_text'          => __('Previous page', 'stratum'),
    'next_text'          => __('Next page', 'stratum'),
    'before_page_number' => '<span class="screen-reader-text">' . __('Page', 'stratum') . ' </span>',
]);
