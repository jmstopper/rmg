<?php

if (!function_exists('yoast_breadcrumb')) {
    return;
}

yoast_breadcrumb('<nav aria-label="Breadcrumb">', '</nav>');
