<?php
if (is_array($args) !== true) {
    return '';
}
?>
<ul class="o-button-list">
    <?php foreach ($args as $key => $button) {
        $atts = [
            'text' => $button['button']['title'],
            'atts' => [
                'href' => $button['button']['url'],
            ]
        ];

        // Add a target if needed
        if ($button['button']['target']) {
            $atts['atts']['target'] = esc_attr($button['button']['target']);
        }
        ?>
        <li>
            <?php echo \Stratum\render('partials/components/button', $atts); ?>
        </li>
    <?php } ?>
</ul>
