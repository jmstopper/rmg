<?php
// Check if flexible content is an array
// If not return early
if (is_array($args)) {
    // Begin looping the blocks
    foreach ($args as $index => $blockArgs) {
        // Store the block key as an argument so we know where we are in
        // the loop
        $blockArgs['_key'] = $index;

        // Modify the block name to be consistent with the naming
        // conventions within the theme
        $blockName = str_replace('_', '-', $blockArgs['acf_fc_layout']);

        // Build a path to the file we want to load
        $path = 'partials/flexible-content/' . $blockName;

        // Load the component and parse the args
        echo \Stratum\render($path, $blockArgs);
    }
}
