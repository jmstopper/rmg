<?php

get_header();

if (have_posts()) {
    while (have_posts()) {
        the_post();

        $overviewArgs = [
            'heading' => __('Overview', 'stratum'),
            'items' => [
                [
                    'heading' => __('Salary', 'stratum'),
                    'content' => get_field('salary'),
                    'icon' => \Stratum\imageURL('salary.svg')
                ],
                // [
                //     'heading' => __('Date Posted', 'stratum'),
                //     'content' => get_the_date(),
                //     'icon' => \Stratum\imageURL('date.svg')
                // ],
                [
                    'heading' => __('Location', 'stratum'),
                    'content' => get_field('location'),
                    'icon' => \Stratum\imageURL('location.svg')
                ]
            ]
        ];

        $overviewArgs['content'] = wpautop('<a class="button" href="#apply">' . __('Apply now', 'rmg') . '</a>');
        $types = get_the_terms(get_the_id(), 'rmg-vacancy-type');

        if (!empty($types)) {
            $types = array_map(function ($type) {
                return $type->name;
            }, $types);

            $overviewArgs['items'][] = [
                'heading' => __('Vacancy Type', 'stratum'),
                'content' => implode(', ', $types),
                'icon' => \Stratum\imageURL('vacancy.svg')
            ];
        }

        $content = \Stratum\render('assets/components/overview', $overviewArgs);
        $content .= apply_filters('the_content', get_the_content());

        if($roleFooter = get_field('role_single_footer', 'option')){
            $content .= '<div id="apply">' . $roleFooter . '</div>';
        }

        $mainContent = \Stratum\render(
            'assets/components/article',
            \Stratum\render('assets/components/wysiwyg', [
                'altstyle' => true,
                'heading' => get_the_title(),
                'content' => $content,
                'meta' => '<a href="' . get_post_type_archive_link(get_post_type()) . '">' . __('jobs', 'stratum') . '</a>'
            ])
        );

        echo \Stratum\render('assets/components/main', $mainContent);
    }
} else {
    echo \Stratum\render(
        'assets/components/main',
        \Stratum\render('partials/wordpress/content-none')
    );
}

get_footer();
