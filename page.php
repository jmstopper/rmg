<?php

get_header();

if (have_posts()) {
    while (have_posts()) {
        the_post();

        $overlayMedia = get_the_post_thumbnail(get_the_id(), 'large');
        $overlayVideo = get_field('overlay_video_mp4_url');

        if ($overlayVideo == '') {
            if ($overlayVideoFile = get_field('overlay_video_mp4')) {
                $overlayVideo = $overlayVideoFile['url'];
            }
        }

        if ($overlayVideo != '') {
            $overlayMedia .= mediaToVideo([
                'autoplay' => true,
                'mp4' => $overlayVideo,
                'fallback' => $overlayMedia
            ]);
        }

        $overlayContent = '';
        if (get_field('overlay_content') !== '') {
            $overlayContent = get_field('overlay_content');
        }

        if ($link = get_field('overlay_link')) {
            $link = '<a href="' . esc_url($link['url']) . '">' . $link['title'] . '</a>';
            $overlayContent .= apply_filters('the_content', $link);
        }

        $content = \Stratum\render('assets/components/overlay', [
            'heading' => trim(get_field('overlay_heading')) != '' ? get_field('overlay_heading') : get_the_title(),
            'meta' => get_field('overlay_tag'),
            'content' => $overlayContent,
            'media' => $overlayMedia,
            'complex' => get_field('overlay_primary'),
        ]);

        $content .= \Stratum\render('assets/components/wysiwyg', [
            'content' => apply_filters('the_content', get_the_content())
        ]);

        echo \Stratum\render('assets/components/main', $content);
    }
} else {
    echo \Stratum\render(
        'assets/components/main',
        \Stratum\render('partials/wordpress/content-none')
    );
}

get_footer();
