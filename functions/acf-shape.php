<?php

add_filter('acf/load_field/name=shape', function ($field) {

    // reset choices
    $field['choices'] = [];

    $shapes = glob(\Stratum\Paths\assetPath('images/shape-*.svg'));
    $choices = [];

    $choices[''] = 'None';

    foreach ($shapes as $key => $shape) {
        $name = basename($shape);
        $friendlyName = $name;

        $friendlyName = str_replace(['.svg', 'shape-'], '', $friendlyName);

        $choices[$name] = ucfirst(trim($friendlyName));
    }

    $field['choices'] = $choices;

    // return the field
    return $field;
});
