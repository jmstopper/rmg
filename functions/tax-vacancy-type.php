<?php

add_action('init', function () {
    register_taxonomy(
        'rmg-vacancy-type',
        ['rmg-role'],
        [
            'public' => false,
            'publicly_queryable' => false, // Not available on the frontend
            'show_in_nav_menus' => true, // Available in admin
            'show_in_rest' => true,
            'show_ui' => true, // Available in admin
            'label' => __('Vacancy Types', 'stratum'),
            'hierarchical' => true,
            'show_admin_column' => true,
            'capabilities'      => [
                'manage_terms'  => 'update_core',
                'edit_terms'    => 'update_core',
                'delete_terms'  => 'update_core',
                'assign_terms'  => 'edit_posts'
            ],
        ]
    );
});
