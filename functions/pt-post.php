<?php

add_filter('manage_post_posts_columns', function ($columns) {

    unset($columns['tags']);
    unset($columns['comments']);

    return array_merge($columns, [
        'custom_author' => __('Custom Author'),
    ]);
});

add_action('manage_post_posts_custom_column', function ($column, $post_id) {
    if ($column === 'custom_author') {
        $author = get_field('author', $post_id);

        if ($author) {
            echo '<a href="' . get_edit_post_link($author) . '">' . get_the_title($author) . '</a>';
        }
    }
}, 10, 2);

add_action('pre_get_posts', function ($query) {
    if (!is_admin() && $query->is_main_query() && $query->is_home()) {
        $query->set('posts_per_page', -1);
    }

    return $query;
});
