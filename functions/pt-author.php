<?php

add_action('init', function () {
    register_post_type('rmg-author', [
        'label' => 'Authors',
        'public' => false, // Not available anywhere
        'publicly_queryable' => false, // Not available on the frontend
        'show_in_nav_menus' => true, // Available in admin
        'show_ui' => true, // Available in admin
        'supports' => ['title', 'editor', 'thumbnail'],
    ]);
});


add_filter('manage_rmg-author_posts_columns', function ($columns) {

    unset($columns['date']);

    return array_merge($columns, [
        'position' => __('Position'),
        'image' => __('Image', 'stratum'),
    ]);
});

add_action('manage_rmg-author_posts_custom_column', function ($column, $post_id) {
    if ($column === 'position') {
        $position = get_field('position', $post_id);

        if ($position) {
            echo $position;
        }
    } elseif ($column === 'image') {
        the_post_thumbnail('thumbnail', $post_id);
    }
}, 10, 2);
