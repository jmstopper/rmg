<?php

add_action('init', function () {
    register_post_type('rmg-role', [
        'label' => 'Roles',
        'public' => true,
        'has_archive' => true,
        'show_in_rest' => true,
        'rewrite' => [
            'slug' => 'find-a-role'
        ],
        'supports' => ['title', 'editor', 'excerpt'],
    ]);
});

add_action('pre_get_posts', function ($query) {
    if (!is_admin() && $query->is_main_query() && is_post_type_archive('rmg-role')) {
        $query->set('posts_per_page', -1);
    }

    return $query;
});

add_action('template_redirect', function () {
    if (is_singular('rmg-role')) {
        global $post;

        $url = get_field('external_url', $post);

        if (!empty($url)) {
            wp_redirect($url);
            exit();
        }
    }
});
