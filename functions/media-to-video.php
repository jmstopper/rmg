<?php

function mediaToVideo(array $args): string
{
    $videoAttributes = '';

    $args = wp_parse_args($args, [
        'autoplay' => false,
        'mp4' => '',
        'poster' => '',
        'fallback' => '',
    ]);

    if ($args['mp4'] == '') {
        return '';
    }

    if ($args['autoplay'] === true) {
        $videoAttributes .= ' autoplay';
        $videoAttributes .= ' loop';
        $videoAttributes .= ' muted';
    } else {
        $videoAttributes .= ' controls';
    }

    if ($args['poster'] != '') {
        $videoAttributes .= ' poster="' . esc_attr($args['poster']) . '"';
    }

    ob_start();

    ?><video <?php echo $videoAttributes; ?>>
        <?php if (!empty($args['mp4'])) { ?>
            <source src="<?php echo $args['mp4']; ?>" type="video/mp4">
        <?php } ?>
        <?php echo $args['fallback']; ?>
    </video><?php

    return ob_get_clean();
}
