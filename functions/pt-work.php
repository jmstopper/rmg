<?php

add_action('init', function () {
    register_post_type('rmg-work', [
        'label' => 'Work',
        'public' => true,
        'show_in_rest' => true,
        'has_archive' => true,
        'rewrite' => [
            'slug' => 'work'
        ],
        'supports' => ['title', 'editor', 'thumbnail', 'excerpt'],
    ]);
});

add_action('pre_get_posts', function ($query) {
    if (!is_admin() && $query->is_main_query() && is_post_type_archive('rmg-work')) {
        $query->set('posts_per_page', -1);

        $featuredItem = get_field('work_featured_item', 'option');

        if ($featuredItem) {
            $query->set('post__not_in', [$featuredItem->ID]);
        }
    }

    return $query;
});
