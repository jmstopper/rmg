<?php

add_action('init', function () {
    register_taxonomy(
        'rmg-work-category',
        ['rmg-work'],
        [
            'public' => true,
            'label' => __('Categories', 'stratum'),
            'hierarchical' => true,
            'show_in_rest' => true,
            'show_admin_column' => true,
            'rewrite' => [
                'with_front' => false,
                'slug' => 'work-type'
            ]
        ]
    );
});

add_action('pre_get_posts', function ($query) {
    if (!is_admin() && $query->is_main_query() && is_tax('rmg-work-category')) {
        $query->set('posts_per_page', -1);

        $featuredItem = get_field('work_featured_item', 'option');

        if ($featuredItem) {
            $query->set('post__not_in', [$featuredItem->ID]);
        }
    }

    return $query;
});
