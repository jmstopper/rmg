<?php

add_action('init', function () {
    register_post_type('rmg-quote', [
        'label' => 'Quotes',
        'public' => false, // Not available anywhere
        'publicly_queryable' => false, // Not available on the frontend
        'show_in_nav_menus' => true, // Available in admin
        'show_ui' => true, // Available in admin
        'supports' => ['title', 'editor', 'thumbnail'],
    ]);
});

add_filter('manage_rmg-quote_posts_columns', function ($columns) {
    unset($columns['date']);

    return array_merge($columns, [
        'quote' => __('Quote', 'stratum'),
        'position' => __('Position', 'stratum'),
    ]);
});

add_action('manage_rmg-quote_posts_custom_column', function ($column, $post_id) {
    if ($column === 'position') {
        $position = get_field('position', $post_id);

        if ($position) {
            echo $position;
        }
    } elseif ($column === 'quote') {
        the_excerpt($post_id);
    }
}, 10, 2);
