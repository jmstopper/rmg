<?php

function theme(): string
{
    global $post;

    // Get the theme for the whole site
    $theme = get_field('theme', 'option');

    // Get the theme from the page
    if(is_singular('page')){
        $color = get_field('theme', $post);

        // If one is set and its not default
        if(!empty($color) && $color !== 'inherit'){
            $theme = $color;
        }
    }

    if(is_null($theme)){
        return 'orange';
    }

    return $theme;
}
