<?php

namespace Stratum\Render;

global $stratumRenderStack;
$stratumRenderStack = [];

function last(): array
{
    $stack = \Stratum\Render\stack();

    if (count($stack) > 0) {
        return array_values(array_slice($stack, -1))[0];
    }

    return [];
}

function stack(): array
{
    global $stratumRenderStack;

    return $stratumRenderStack;
}

function push(string $path, $args = ''): void
{
    global $stratumRenderStack;

    $stratumRenderStack[] = [
        'path' => $path,
        'args' => $args
    ];
}

function pop(): void
{
    global $stratumRenderStack;

    array_pop($stratumRenderStack);
}
