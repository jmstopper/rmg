<?php
/**
 * Template Name: Flexible Content
 */
get_header(); ?>

<main id="content">
    <?php if (have_posts()) { ?>
        <?php while (have_posts()) {
            the_post();
            the_content();
        } ?>
    <?php } else { ?>
        <?php echo \Stratum\render('partials/wordpress/content-none'); ?>
    <?php } ?>
</main>

<?php get_footer(); ?>
