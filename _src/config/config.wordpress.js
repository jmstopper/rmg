export default {
    "Theme Name": "RMG",
    "Theme URI": "https://minno.agency/",
    "Author": "Minno",
    "Author URI": "https://minno.agency/",
    "Description": "Custom theme developed RMG",
    "Licence": "GNU General Public License v2 or later",
    "License URI": "http://www.gnu.org/licenses/gpl-2.0.html",
    "Text Domain": "stratum",
}
