import paths from './config.paths';
import plugins from './config.plugins';
import wordpress from './config.wordpress';

export default {
    paths: paths,
    plugins: plugins,
    wordpress: wordpress
}
