<?php if(!empty($args['networks'])) { ?>
    <div class="social-icons">
        <?php if (!empty($args['heading'])) { ?>
            <div class="social-icons__heading">
                <?= $args['heading']; ?>
            </div>
        <?php } ?>

        <ul class="social-icons__icons">
            <?php foreach ($args['networks'] as $key => $network) { ?>
                <li>
                    <a target="_blank" rel="noopener noreferrer" href="<?= $network['url']; ?>">
                        <?= \Stratum\svg($network['network']); ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>
