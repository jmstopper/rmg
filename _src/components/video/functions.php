<?php

add_filter('stratum/render/assets/components/video', function (array $args): array {

    $args = wp_parse_args($args, [
        'id' => '',
    ]);

    if (!empty($args['tag'])) {
        $args['meta'] = $args['tag'];
        unset($args['tag']);
    }

    if ($args['source'] === 'local') {
        $fallback = '';

        if (!empty($args['fallback'])) {
            $fallback = wp_get_attachment_image($args['fallback']['id'], 'large');
        }

        $args['video'] = mediaToVideo([
            'mp4' => $args['mp4']['url'],
            'autoplay' => $args['autoplay'],
            'poster' => $args['fallback']['sizes']['large'],
            'fallback' => $fallback,
        ]);

        unset($args['mp4']);
    } elseif ($args['remote']) {
        $args['video'] = $args['remote'];
        unset($args['remote']);
    } elseif ($args['source'] === 'url') {
        $fallback = '';

        if (!empty($args['fallback'])) {
            $fallback = wp_get_attachment_image($args['fallback']['id'], 'large');
        }

        $args['video'] = mediaToVideo([
            'mp4' => $args['url'],
            'autoplay' => $args['autoplay'],
            'poster' => $args['fallback']['sizes']['large'],
            'fallback' => $fallback,
        ]);
    }

    return $args;
});
