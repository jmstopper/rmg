<?php

$args = wp_parse_args($args, [
    'video' => '',
    'clean' => false,
]);

$classes = ['video'];
$videoClasses = ['video__video'];

if ($args['clean'] !== true) {
    $classes[] = 'video--style';
    $classes[] = 'shape';
}

if ($args['source'] === 'remote') {
    $videoClasses[] = 'responsive-embed';
}

?><div class="<?= esc_attr(implode(' ', $classes)); ?>" style="--shapeurl:<?= esc_attr($args['shape']); ?>" id="<?= esc_attr($args['id']); ?>">
    <?php if (!empty($args['heading'])) { ?>
        <h2 class="video__heading">
            <?= $args['heading']; ?>
        </h2>
    <?php } ?>

    <?php if (!empty($args['meta'])) { ?>
        <div class="video__meta">
            <?= \Stratum\render('assets/components/tag', $args['meta']); ?>
        </div>
    <?php } ?>

    <?php if ($args['content'] != '') { ?>
        <div class="video__content">
            <?= $args['content']; ?>
        </div>
    <?php } ?>

    <div class="<?= esc_attr(implode(' ', $videoClasses)); ?>">
        <?= $args['video']; ?>
    </div>
</div>
