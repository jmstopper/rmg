// -----------------------------------------------------------------------------
// Import all the libraries we need
// -----------------------------------------------------------------------------
import Headroom from "headroom.js";
import clickOutside from '../../scripts/helpers/clickOutside';

// -----------------------------------------------------------------------------
// Define all the constants we will be using in this file
// -----------------------------------------------------------------------------
const mq = window.matchMedia("(min-width: 768px)");
const escKeycode = 27;
const header = document.querySelector('.header');
const body = document.querySelector('body');
const dropdownSelector = '.menu-item-has-children';
const navigation = header.querySelector('.header__navigation');
const dropdowns = header.querySelectorAll(dropdownSelector);
const burger = header.querySelector('.js-header-burger');
const menuHeadings = header.querySelectorAll('.menu-heading');

// -----------------------------------------------------------------------------
// Startup Headroom JS
// -----------------------------------------------------------------------------
if (mq.matches) {
    new Headroom(header, {
        offset: 50,
        tolerance: 0,
        onUnpin: () => {
            closeDropdown();
        }
    }).init();
}

// -----------------------------------------------------------------------------
// Make the burger work
// -----------------------------------------------------------------------------
burger.addEventListener('click', () => {
    burger.classList.toggle('active');
    body.classList.toggle('mobile-menu-open');

    closeDropdown();
});

for (const menuHeading of menuHeadings) {
    menuHeading.addEventListener('click', () => {
        menuHeading.closest('.menu-item-has-children').classList.remove('active');
    });
}

// -----------------------------------------------------------------------------
// Detect a click outside the menu to close submenu items
// -----------------------------------------------------------------------------
document.addEventListener('click', (event) => {
    if (header.classList.contains('active') && clickOutside(event, header)) {
        // Close all the dropdowns
        for (const dropdown of dropdowns) {
            closeDropdown(dropdown);
        }
    }
});

// -----------------------------------------------------------------------------
// Handle opening and closing the submenus
// -----------------------------------------------------------------------------
for (const dropdown of dropdowns) {
    const link = dropdown.querySelector('a');
    const submenu = dropdown.querySelector('.sub-menu');

    // Lets detect if there actually is a sub menu. There are edge cases where
    // there might not be and then we would disable a link when we shouldn't
    if (submenu !== null) {

        link.addEventListener('click', (event) => {
            event.preventDefault();
            togglDropdown(dropdown, submenu);
        });

        document.addEventListener('keyup', (event) => {
            if (event.keyCode === escKeycode) {

                const activeElement = document.activeElement;
                const activeDropdown = activeElement.closest(dropdownSelector);
                const activeLink = activeDropdown.querySelector('a');

                // Return focus to the parent link
                activeLink.focus();

                // close the dropdown
                closeDropdown();
            }
        });
    }
}

// -----------------------------------------------------------------------------
// Open or close the submenu based on its current state
// -----------------------------------------------------------------------------
function togglDropdown(dropdown, submenu) {
    if (dropdown.classList.contains('active')) {
        closeDropdown();
    } else {
        closeDropdown();
        openDropdown(dropdown, submenu);
    }
}

// -----------------------------------------------------------------------------
// Open a specific dropdown
// -----------------------------------------------------------------------------
function openDropdown(dropdown, submenu) {
    // We should be able to do this only once but with fonts reloading
    // and adjusting the width of the nav bar its messing with the
    // layout. Therefore lets do it every time we open the menu
    submenu.style.maxWidth = navigation.offsetWidth + "px";

    // Make the item visible
    header.classList.add('active');
    dropdown.classList.add('active');
}

// -----------------------------------------------------------------------------
// Close a dropdown
// -----------------------------------------------------------------------------
function closeDropdown() {
    // Make the item invisible
    header.classList.remove('active');

    for (const dropdown of dropdowns) {
        dropdown.classList.remove('active');
    }
}
