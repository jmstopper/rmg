<?php

$classes = ['header'];

if (get_post_type() === 'page' && !is_404()
   || is_home()
   || is_category()
   || is_post_type_archive('rmg-work')
   || is_tax(['rmg-work-category'])
   || is_search()
) {
    $classes[] = 'header--overlay';
}

?><header class="<?= esc_attr(implode(' ', $classes)); ?>">
    <?= \Stratum\render('assets/components/skip-link'); ?>

    <div class="header__inner">
        <div class="header__top">
            <a class="header__logo" href="<?= esc_url(get_home_url()); ?>">
                <?= \Stratum\render('assets/components/logo', [
                    'override' => true
                ]); ?>
            </a>

            <?php wp_nav_menu([
                'theme_location'  => 'mobile',
                'depth'           => 1,
                'container'       => '',
                'container_class' => '',
                'menu_class'      => 'header__mobile-menu',
                'fallback_cb'     => false,
            ]); ?>

            <?= \Stratum\render('assets/components/burger', [
                'class' => 'header__burger js-header-burger'
            ]); ?>
        </div>

        <nav class="header__navigation" role="navigation" aria-label="<?php esc_attr_e('Header navigation', 'stratum'); ?>">
            <?php wp_nav_menu([
                'theme_location'  => 'header',
                'depth'           => 2,
                'container'       => '',
                'container_class' => '',
                'menu_class'      => 'header__menu',
                'fallback_cb'     => false,
                'walker' => new RMGCustomMenuWalker,
            ]); ?>

            <?php wp_nav_menu([
                'theme_location'  => 'topheader',
                'depth'           => 2,
                'container'       => '',
                'container_class' => '',
                'menu_class'      => 'header__top-menu',
                'fallback_cb'     => false,
                'walker' => new RMGCustomMenuWalker,
            ]); ?>
        </nav>
    </div>

</header>
