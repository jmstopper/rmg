<?php

class RMGCustomMenuWalker extends Walker_Nav_Menu
{
    function start_lvl(&$output, $depth = 0, $args = [])
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<div class='sub-menu-wrap'><div class='sub-menu-inner'><ul class='sub-menu'>\n";
    }

    function end_lvl(&$output, $depth = 0, $args = [])
    {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul></div></div>\n";
    }
}

add_filter('wp_nav_menu_objects', function ($items, $args) {

    foreach ($items as &$item) {
        // ---------------------------------------------------------------------
        // Handle making links button, this in only on theme_location header
        // ---------------------------------------------------------------------
        $style = get_field('style', $item);

        if ($style != '' && $style !== 'link') {
            $item->classes[] = 'menu-button menu-button--' . $style;
        }

        // ---------------------------------------------------------------------
        // Handle making headings, this in only on theme_location topheader
        // ---------------------------------------------------------------------
        if (get_field('heading', $item) === true) {
            $item->classes[] = 'menu-heading';
        }

        // ---------------------------------------------------------------------
        // Add icons if needed
        // ---------------------------------------------------------------------
        if (get_field('phone', $item) === true) {
            $item->classes[] = 'menu-icon-phone';
        }
    }

    // return
    return $items;
}, 10, 2);
