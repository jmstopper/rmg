<?php

$class = ['iconcards'];

if (!empty($args['class'])) {
    $class[] = $args['class'];
}

if (!empty($args['cards'])) {
    ?><div class="<?= esc_attr(implode(' ', $class)); ?>" id="<?= esc_attr($args['id']); ?>">

        <?php if (!empty($args['heading'])) { ?>
            <h2 class="iconcards__heading">
                <?= $args['heading']; ?>
            </h2>
        <?php } ?>

        <?php if (!empty($args['meta'])) { ?>
            <div class="iconcards__meta">
                <?= \Stratum\render('assets/components/tag', $args['meta']); ?>
            </div>
        <?php } ?>

        <div class="iconcards__row">
            <?php foreach ($args['cards'] as $key => $card) { ?>
                <div class="iconcards__col">
                    <?= \Stratum\render('assets/components/iconcard', $card); ?>
                </div>
            <?php } ?>
        </div>
    </div>

<?php }
