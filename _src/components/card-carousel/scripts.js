import CardCarousel from './block';

const blocks = document.querySelectorAll('.card-carousel');

for(const block of blocks) {
    new CardCarousel(block);
}
