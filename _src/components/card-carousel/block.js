export default class CardCarousel {
    constructor(element) {
        this.wrapper = element;

        this.items = this.wrapper.querySelector('.card-carousel__carousel');
        this.backwardButton = this.wrapper.querySelector('.is-previous');
        this.forwardButton = this.wrapper.querySelector('.is-next');
        this.init();
    }

    init(){
        this.forwardButton.addEventListener('click', () => {
            this.forward();
        });

        this.backwardButton.addEventListener('click', () => {
            this.backward();
        });

        this.items.addEventListener('scroll', (event) => {
            const scrollOffset = event.target.scrollLeft;

            if(scrollOffset === 0) {
                this.backwardButton.disabled = true;
                this.forwardButton.disabled = false;
            }else if(scrollOffset === this.items.scrollWidth - (this.items.scrollWidth / this.items.children.length)) {
                this.backwardButton.disabled = false;
                this.forwardButton.disabled = true;
            }else {
                this.backwardButton.disabled = false;
                this.forwardButton.disabled = false;
            }
        });
    }

    forward() {
        this.items.scrollTo({
            top: 0,
            left: this.items.scrollLeft + (this.items.scrollWidth / this.items.children.length),
            behavior: 'smooth'
        });
    }

    backward() {
        this.items.scrollTo({
            top: 0,
            left: this.items.scrollLeft - (this.items.scrollWidth / this.items.children.length),
            behavior: 'smooth'
        });
    }
}
