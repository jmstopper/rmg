<?php

add_filter('stratum/render/assets/components/card-carousel', function (array $args): array {

    $args = wp_parse_args($args, [
        'id' => '',
        'meta' => '',
        'heading' => '',
        'content' => '',
        'cards' => [],
    ]);

    if (!empty($args['tag'])) {
        $args['meta'] = $args['tag'];
        unset($args['tag']);
    }

    $args['cards'] = array_map(function($card){

        if(!empty($card['image']['id'])){
            $card['media'] = wp_get_attachment_image($card['image']['id'], 'medium');
            unset($card['image']);
        }

        if(!empty($card['icon']['id'])){
            $card['icon'] = wp_get_attachment_image($card['icon']['id'], 'medium');
        }

        if (!empty($card['link']['title'])) {
            $card['content'] .= '<p class="card-carousel__card-link" aria-hidden="true"><span>' . $card['link']['title'] . '</span></p>';
        }

        return $card;
    }, $args['cards']);

    return $args;
});
