<div class="card-carousel" id="<?= esc_attr($args['id']); ?>">

    <?php if (!empty($args['heading']) || !empty($args['meta']) || !empty($args['content'])) { ?>
        <div class="card-carousel__header">
            <?php if (!empty($args['heading'])) { ?>
                <h2 class="card-carousel__heading">
                    <?= $args['heading']; ?>
                </h2>
            <?php } ?>

            <?php if (!empty($args['meta'])) { ?>
                <div class="card-carousel__meta">
                    <?= \Stratum\render('assets/components/tag', $args['meta']); ?>
                </div>
            <?php } ?>

            <?php if (!empty($args['content'])) { ?>
                <div class="card-carousel__content">
                    <?= $args['content']; ?>
                </div>
            <?php } ?>
        </div>
    <?php } ?>

    <div class="card-carousel__carousel">
        <?php foreach ($args['cards'] as $key => $card) { ?>
            <div class="card-carousel__card">
                <?php if (!empty($card['heading'])) { ?>
                    <h3 class="card-carousel__card-heading">
                        <?php if (!empty($card['link']['url'])) { ?>
                            <a href="<?= esc_attr($card['link']['url']); ?>">
                        <?php } ?>

                        <?= $card['heading']; ?>

                        <?php if (!empty($card['link']['url'])) { ?>
                            </a>
                        <?php } ?>
                    </h3>
                <?php } ?>

                <?php if (!empty($card['icon'])) { ?>
                    <div class="card-carousel__card-icon img-fit is-contain" aria-hidden="true">
                        <?= $card['icon']; ?>
                    </div>
                <?php } ?>

                <?php if (!empty($card['content'])) { ?>
                    <div class="card-carousel__card-content">
                        <?= $card['content']; ?>
                    </div>
                <?php } ?>

                <?php if (!empty($card['media'])) { ?>
                    <div class="card-carousel__card-media img-fit">
                        <?= $card['media']; ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>

    <div class="card-carousel__controls">
        <button class="card-carousel__control is-previous" aria-hidden="true">
            <span class="card-carousel__arrow"></span>
            <span class="screen-reader-text"><?= __('Next slide', 'rmg'); ?></span>
        </button>

        <button class="card-carousel__control is-next" aria-hidden="true">
            <span class="card-carousel__arrow"></span>
            <span class="screen-reader-text"><?= __('Next slide', 'rmg'); ?></span>
        </button>
    </div>
</div>
