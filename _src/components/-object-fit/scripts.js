const selectorClass = '.img-fit';
const fallbackClass = 'img-fit--fallback';
const objectFitIsSupported = () => 'objectFit' in document.documentElement.style;

if (objectFitIsSupported() !== true) {
    const imgWrappers = document.querySelectorAll(selectorClass);

    for (const wrapper of imgWrappers) {
        const img = wrapper.querySelector('img');
        const src = img.dataset.src !== undefined && img.dataset.src !== '' ? img.dataset.src : img.src;

        if (src) {
            wrapper.style.backgroundImage = 'url("' + src + '")';
            wrapper.classList.add(fallbackClass);
        }
    }
}
