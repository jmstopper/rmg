<div class="<?= esc_attr(implode(' ', $args['classes'])); ?>">
    <?= $args['text']; ?>
</div>
