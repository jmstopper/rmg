<?php

add_filter('stratum/render/assets/components/tag', function ($args): array {

    $args = [
        'classes' => ['tag'],
        'text' => $args,
    ];

    $tagIcon = get_field('tag_icon', 'option');

    if(is_singular('page')){
        // We need to pass the id because when this code is run, its in the context
        // of an ACF Gutenberg block
        $icon = get_field('tag_icon', get_the_id());

        if(!empty($icon) && $icon !== 'inherit'){
            $tagIcon = $icon;
        }
    }

    $args['classes'][] = 'is-' . $tagIcon;

    return $args;
});
