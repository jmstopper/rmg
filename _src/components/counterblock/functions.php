<?php

add_filter('stratum/render/assets/components/counterblock', function (array $args): array {

    $args = wp_parse_args($args, [
        'id' => '',
    ]);

    if (!empty($args['image']['id'])) {
        $args['media'] = wp_get_attachment_image($args['image']['id'], 'full');
        unset($args['image']);
    }

    if (!empty($args['mobile_image']['id'])) {
        $args['media_mobile'] = wp_get_attachment_image($args['mobile_image']['id'], 'large');
        unset($args['mobile_image']);
    }

    if (!empty($args['link'])) {
        $button = '<a class="button" href="' . esc_url($args['link']['url']) . '">' . $args['link']['title'] . '</a>';
        $args['footer'] = apply_filters('the_content', $button);
    }

    return $args;
});
