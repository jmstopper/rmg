<?php

$classes = ['counterblock'];

if (!empty($args['class'])) {
    $classes[] = $args['class'];
}

?><div class="<?= esc_attr(implode(' ', $classes)); ?>" id="<?= esc_attr($args['id']); ?>">
    <div class="counterblock__wrap">
        <div class="counterblock__inner">
            <?php if (!empty($args['heading'])) { ?>
                <h2 class="counterblock__heading">
                    <?= $args['heading']; ?>
                </h2>
            <?php } ?>

            <?php if (!empty($args['number'])) { ?>
                <div class="counterblock__counter">
                    <span class="js-counter" data-number="<?= esc_attr($args['number']); ?>">
                        <?= $args['number']; ?>
                    </span>
                    <div class="counterblock__counter-images img-fit">
                        <?= \Stratum\image('shape-circle-1.svg'); ?>
                        <?= \Stratum\image('shape-circle-2.svg'); ?>
                        <?= \Stratum\image('shape-circle-3.svg'); ?>
                        <?= \Stratum\image('shape-circle-4.svg'); ?>
                    </div>
                </div>
            <?php } ?>
        </div>

        <?php if (!empty($args['footer'])) { ?>
            <div class="counterblock__footer">
                <?= $args['footer']; ?>
            </div>
        <?php } ?>

        <?php if (!empty($args['media'])) { ?>
            <div class="counterblock__media img-fit">
                <?php
                echo $args['media'];

                if (!empty($args['media_mobile'])) {
                    echo $args['media_mobile'];
                }

                ?>
            </div>
        <?php } ?>
    </div>
</div>
