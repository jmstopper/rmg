import { CountUp } from 'countup.js';
import throttle from '../../scripts/helpers/throttle';

const isInViewport = (e, { top: t, height: h } = e.getBoundingClientRect()) => t <= innerHeight && t + h >= 0;
const blocks = document.querySelectorAll('.counterblock');

for (const block of blocks) {
    let started = false;
    const counterEl = block.querySelector('.js-counter');
    const number = counterEl.dataset.number;
    const countUp = new CountUp(counterEl, number, {
        duration: 3
    });

    window.addEventListener('scroll', throttle(() => {
        if (started === false && isInViewport(counterEl)) {
            countUp.start();
            started = true;
        }
    }, 500));
}
