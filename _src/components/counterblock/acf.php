<?php

acf_register_block_type([
    // Name in code, alphabetical only
    'name' => 'counterblock',

    // Name for Interface
    'title' =>  'Counter Block',

    // Short description
    'description' => 'Counter Block',

    // common | formatting | layout | widgets | embed
    'category' => 'layout',

    // An array of post types that this block will be available for
    'post_types' => ['page'],

    // When the block is clicked on, the block is replaced with the fields
    'mode' => 'auto',

    // The default block alignment.
    // 'left', 'center', 'right', 'wide', 'full'
    'align' => 'full',

    // What options do we want this block to support.
    'supports' => [
        'anchor' => true,
        // Align can be an array of options, or a boolean
        'align' => false,
    ],

    // Handle rendering the block
    'render_callback' => function ($block) {
        $args = \Stratum\Wordpress\Gutenberg::classes(get_fields(), $block);

        echo \Stratum\render('assets/components/counterblock', $args);
    }
]);
