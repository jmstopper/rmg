<?php

add_filter('stratum/render/assets/components/iconlist', function (array $args): array {

    $args['items'] = array_map(function ($item) {
        $item['icon'] = 'url(' . \Stratum\imageURL($item['type'] . '.svg') . ')';
        unset($item['type']);

        return $item;
    }, $args['items']);

    return $args;
});
