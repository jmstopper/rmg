<ul class="icon-list">
    <?php foreach ($args['items'] as $key => $item) { ?>
        <li style="--iconurl:<?= esc_attr($item['icon']); ?>">
            <?php if (!empty($item['link'])) { ?>
                <a href="<?= esc_url($item['link']); ?>">
            <?php } ?>

            <?= $item['text']; ?>

            <?php if (!empty($item['link'])) { ?>
                </a>
            <?php } ?>
        </li>
    <?php } ?>

</ul>
