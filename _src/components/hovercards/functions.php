<?php

add_filter('stratum/render/assets/components/hovercards', function (array $args): array {

    $args = wp_parse_args($args, [
        'id' => '',
    ]);

    if (!empty($args['tag'])) {
        $args['meta'] = $args['tag'];
        unset($args['tag']);
    }

    return $args;
});
