<?php

$class = ['hover-cards'];

if (!empty($args['class'])) {
    $class[] = $args['class'];
}

?><div class="<?= esc_attr(implode(' ', $class)); ?>" id="<?= esc_attr($args['id']); ?>">
    <?php if (!empty($args['heading'])) { ?>
        <h2 class="hover-cards__heading">
            <?= $args['heading']; ?>
        </h2>
    <?php } ?>

    <?php if (!empty($args['meta'])) { ?>
        <div class="hover-cards__meta">
            <?= \Stratum\render('assets/components/tag', $args['meta']); ?>
        </div>
    <?php } ?>

    <div class="hover-cards__row">
        <?php foreach ($args['cards'] as $key => $card) { ?>
            <div class="hover-cards__col">
                <?= \Stratum\render('assets/components/hovercard', $card); ?>
            </div>
        <?php } ?>
    </div>
</div>
