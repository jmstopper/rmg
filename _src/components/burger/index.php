<?php
// Merge default arguments
$args = array_replace([
    'class' => ''
], $args);

// Add required arguments
$args['class'] = 'burger ' . $args['class'];

// Markup
?><button class="<?= esc_attr(trim($args['class'])); ?>">
    <span class="burger__line burger__line--1"></span>
    <span class="burger__line burger__line--2"></span>
    <span class="burger__line burger__line--3"></span>
</button>
