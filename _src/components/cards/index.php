<?php

$class = ['cards'];

if (!empty($args['class'])) {
    $class[] = $args['class'];
}

if (!empty($args['alternatecards'])) {
    $class[] = 'cards--alternatecards';
}

if (!empty($args['featured']) && $args['featured'] === true) {
    $class[] = 'cards--featured';
}

if (empty($args['heading'])) {
    $class[] = 'cards--skinny-header';
}

?><div class="<?= esc_attr(implode(' ', $class)); ?>" id="<?= esc_attr($args['id']); ?>">

    <?php if (!empty($args['heading']) || !empty($args['meta']) || !empty($args['content'])) { ?>
        <div class="cards__header">
            <?php if (!empty($args['heading'])) { ?>
                <h2 class="cards__heading">
                    <?= $args['heading']; ?>
                </h2>
            <?php } ?>

            <?php if (!empty($args['meta'])) { ?>
                <div class="cards__meta">
                    <?= \Stratum\render('assets/components/tag', $args['meta']); ?>
                </div>
            <?php } ?>

            <?php if (!empty($args['content'])) { ?>
                <div class="cards__content">
                    <?= $args['content']; ?>
                </div>
            <?php } ?>
        </div>
    <?php } ?>

    <div class="cards__row-wrap">
        <div class="cards__row">
            <?php foreach ($args['items'] as $key => $card) {
                $classes = ['cards__col'];

                if ($card instanceof WP_Post) {
                    $terms = [];

                    if (get_post_type($card) === 'rmg-work') {
                        $terms = get_the_terms($card, 'rmg-work-category');
                    } elseif (get_post_type($card) === 'post') {
                        $terms = get_the_terms($card, 'category');
                    }

                    if (!empty($terms)) {
                        $terms = array_map(function ($term) {
                            return $term->slug;
                        }, $terms);

                        $classes = array_merge($classes, $terms);
                    }
                }
                ?>
                <div class="<?= implode(' ', $classes); ?>">
                    <?= \Stratum\render('assets/components/card', $card); ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <?php if (!empty($args['footer'])) { ?>
        <div class="cards__footer">
            <?= $args['footer']; ?>
        </div>
    <?php } ?>
</div>
