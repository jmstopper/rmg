if (document.querySelector('body').classList.contains('post-type-archive-rmg-work')) {
    const cards = document.querySelectorAll('.cards__col');
    const filters = document.querySelectorAll('.pill');

    for (const filter of filters) {
        if (filter.dataset.term !== '') {
            filter.addEventListener('click', (event) => {
                event.preventDefault();

                for (const filter of filters) {
                    filter.classList.remove('active');
                }

                filter.classList.add('active');

                for (const card of cards) {
                    if (filter.dataset.term === 'all' || card.classList.contains(filter.dataset.term)) {
                        card.style.display = 'block';
                    } else {
                        card.style.display = 'none';
                    }
                }
            });
        }
    }
}
