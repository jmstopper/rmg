<?php

add_filter('stratum/render/assets/components/cards', function (array $args): array {

    $args = wp_parse_args($args, [
        'id' => '',
    ]);

    if (!empty($args['source']) && $args['source'] === 'latest') {
        $args['items'] = get_posts([
            'posts_per_page' => '2',
            'post_type' => $args['post_type'],
        ]);
    }

    if (!empty($args['tag'])) {
        $args['meta'] = $args['tag'];
        unset($args['tag']);
    }

    // -------------------------------------------------------------------------
    // Convert the cards argument to an items argument
    // -------------------------------------------------------------------------
    if (!empty($args['cards'])) {
        $args['items'] = $args['cards'];
        unset($args['cards']);
    }

    if (!empty($args['link'])) {
        $button = '<a class="button" href="' . esc_url($args['link']['url']) . '">' . $args['link']['title'] . '</a>';
        $args['footer'] = apply_filters('the_content', $button);
        unset($args['link']);
    }

    return $args;
});
