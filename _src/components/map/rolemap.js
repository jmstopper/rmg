import L from 'leaflet';

class RoleMap {
    constructor(map, roles) {
        if (map !== null) {
            this.active = true;
            this.mapEl = map.querySelector('.map__map');
            this.markers = [];

            // ---------------------------------------------------------------------
            // Initialise the map
            // ---------------------------------------------------------------------
            this.initMap();

            // ---------------------------------------------------------------------
            // Add the markers to the map
            // ---------------------------------------------------------------------
            this.initMarkers(roles);

            // ---------------------------------------------------------------------
            // Make the map fit the markers
            // ---------------------------------------------------------------------
            this.fitMapToMarkers(this.markers);
        } else {
            this.active = false;
        }
    }

    filterMarkers(roleIds) {
        if (this.active === true) {
            const filteredMarkers = [];

            for (let marker of this.markers) {

                const intersection = roleIds.filter(element => marker.options.location.roleids.includes(element));

                if (intersection.length > 0) {
                    marker.setOpacity(1);
                    filteredMarkers.push(marker);
                } else {
                    marker.setOpacity(0);
                }
            }

            this.fitMapToMarkers(filteredMarkers, true);
        }
    }

    fitMapToMarkers(markers, pan) {
        if (markers.length > 0) {
            // Make a group from the markers
            const group = new L.featureGroup(markers);

            if (pan === true) {
                this.map.flyToBounds(group.getBounds());
            } else {
                this.map.fitBounds(group.getBounds());
            }
        }
    }

    initMarkers(roles) {
        const locations = {};

        for (let role of roles) {

            // -------------------------------------------------------------
            // Check if the location exists on the locations object
            // -------------------------------------------------------------
            if(role.latlnghash !== ''){
                if (role.latlnghash in locations !== true) {
                    // -------------------------------------------------------------
                    // Create the locations and add a place for the roles
                    // -------------------------------------------------------------
                    locations[role.latlnghash] = {
                        roles: [],
                        lat: role.lat,
                        lng: role.lng,
                        pin: role.pin,
                    };
                }

                locations[role.latlnghash].roles.push(role);
            }
        }

        for (let locationKey in locations) {
            const location = locations[locationKey];

            // Generate the content for the info window
            let content = '<div class="infowindow"><ul class="infowindow__roles">';

            for (const role of location.roles) {
                content += '<li><a href="' + role.link + '">' + role.heading + '</a></li>';
            }

            content += '</ul></div>';

            location.roleids = location.roles.map((role) => {
                return role.id;
            });

            if (this.isValidCoordinates(location.lat, location.lng)) {
                const marker = L.marker([location.lat, location.lng], {
                    location: location,
                    minWidth: 300,
                    icon: L.icon({
                        iconUrl: location.pin,
                        iconSize: [30, 40], // size of the icon
                        iconAnchor: [15, 39], // point of the icon which will correspond to marker's location
                    })
                }).bindPopup(content).addTo(this.map);

                this.markers.push(marker);
            }
        }
    }

    initMap() {
        this.map = L.map(this.mapEl, {
            // Positioning and zoom
            maxZoom: 10,

            // Add the tiles
            layers: [L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {})],

            // Control interactivity
            scrollWheelZoom: false,
        });

        this.map.setView(new L.LatLng(51.5074, 0.1278), 6);
    }

    isValidCoordinates(lat, lng) {
        return this.longitude(lng) && this.latitude(lat);
    }

    longitude(lng) {
        return !!(this.isNumber(lng) && this.between(lng, -180, 180));
    }

    latitude(lat) {
        return !!(this.isNumber(lat) && this.between(lat, -90, 90));
    }

    isNumber(n) {
        return typeof n === 'number';
    }

    between(test, low, high) {
        if (test >= low && test <= high) {
            return true;
        }

        return false;
    }

}

export default RoleMap;
