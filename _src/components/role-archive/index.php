<?php

$rolesID = 'roles' . uniqid();

$classes = ['role-archive'];

if ($args['map'] === true) {
    $classes[] = 'role-archive--map';
}

?><div id="<?= $rolesID; ?>" class="<?= implode(' ', $classes); ?>">
    <?= \Stratum\render('assets/components/roles-list', $args); ?>

    <?php if ($args['map'] === true) {
        echo \Stratum\render('assets/components/map', [
            'pins' => $args['roles']
        ]);
    } ?>

    <script>window.<?= $rolesID; ?> = <?= json_encode($args['roles']); ?></script>
</div>
