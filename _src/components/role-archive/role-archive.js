import RoleMap from '../map/rolemap';

class RoleArchive {
    constructor(element) {
        this.element = element;
        this.id = element.id;
        this.roles = window[this.id];
        this.filters = {};
        this.none = element.querySelector('.roles-list__none');
        this.flexibleHeading = element.querySelector('.is-flexible-heading');
        this.fixedHeading = element.querySelector('.is-fixed-heading');

        // ---------------------------------------------------------------------
        // Construct meta data about the roles
        // ---------------------------------------------------------------------
        this.initMeta();

        // ---------------------------------------------------------------------
        // Role list things
        // ---------------------------------------------------------------------
        this.roleListElements = this.element.querySelectorAll('.roles-list-item');

        // ---------------------------------------------------------------------
        // Initialise the map
        // ---------------------------------------------------------------------
        this.map = new RoleMap(this.element.querySelector('.map'), this.roles);

        // ---------------------------------------------------------------------
        // Start the show
        // ---------------------------------------------------------------------
        this.filteredRoles = this.filterRoles();
        this.eventListeners();
    }

    initMeta() {
        for (let i = this.roles.length - 1; i >= 0; i--) {
            this.roles[i].search = (
                this.roles[i].heading + " " +
                this.roles[i].excerpt + " " +
                this.roles[i].brand_company + " " +
                this.roles[i].salary
            ).toLowerCase();
        }
    }

    eventListeners() {
        const form = this.element.querySelector('form');
        const keyword = this.element.querySelector('.js-keyword');
        const filterElements = this.element.querySelectorAll('.js-filter');

        if (form !== null) {
            form.addEventListener('reset', () => {
                this.filters = {};
                this.filterRoles();
            });
        }

        if (keyword !== null) {
            keyword.addEventListener('keyup', () => {
                this.filters.keyword = keyword.value;
                this.filterRoles();
            });
        }

        if (filterElements.length > 0) {
            for (const filter of filterElements) {
                filter.addEventListener('change', () => {
                    this.filters[filter.dataset.slug] = filter.value;
                    this.filterRoles();
                });
            }
        }
    }

    filterRoles() {
        this.filteredRoles = this.roles.filter(role => {
            // -----------------------------------------------------------------
            // Filter by keyword
            // -----------------------------------------------------------------
            if (this.filters.keyword !== undefined && this.filters.keyword !== '') {

                // Join the heading and the content as we want to search through
                // both. If the string does not contain the keyword. Hide the entry
                if (role.search.includes(this.filters.keyword.toLowerCase()) !== true) {
                    return false;
                }
            }

            // -----------------------------------------------------------------
            // Filter by location
            // -----------------------------------------------------------------
            if (Object.keys(this.filters).length > 0) {
                // Get the key and the value of the filter
                for (const [name, value] of Object.entries(this.filters)) {

                    // Check we are not looking at the keyword
                    if (name != 'keyword' && value != '') {
                        let match = false;

                        // Loop each of the terms the role has
                        for (const term of role[name]) {
                            // if the role has the term
                            if (term.slug === value) {
                                match = true;
                                break;
                            }
                        }

                        // If there was no match hide this job
                        if (match === false) {
                            return false;
                        }
                    }
                }
            }

            return true;
        });

        this.render();
    }

    render() {
        const filteredRoleIDs = this.filteredRoles.map(role => role.id);
        let fixedShown = 0;
        let flexibleShown = 0;

        // ---------------------------------------------------------------------
        // Hide and show elements in the role list
        // ---------------------------------------------------------------------
        for (let roleEl of this.roleListElements) {
            if (filteredRoleIDs.indexOf(parseInt(roleEl.dataset.roleId)) !== -1) {
                roleEl.classList.remove('hidden');

                if (roleEl.classList.contains('is-fixed')) {
                    fixedShown += 1;
                } else {
                    flexibleShown += 1;
                }
            } else {
                roleEl.classList.add('hidden');
            }
        }

        console.log(fixedShown, flexibleShown);

        if (this.flexibleHeading) {
            if (flexibleShown > 0) {
                this.flexibleHeading.classList.remove('hidden');
            } else {
                this.flexibleHeading.classList.add('hidden');
            }
        }

        if (this.fixedHeading) {
            if (fixedShown > 0) {
                this.fixedHeading.classList.remove('hidden');
            } else {
                this.fixedHeading.classList.add('hidden');
            }
        }

        if (filteredRoleIDs.length === 0) {
            this.none.classList.add('active');
        } else {
            this.none.classList.remove('active');
        }

        // ---------------------------------------------------------------------
        // Hide and show elements on the map
        // ---------------------------------------------------------------------
        this.map.filterMarkers(filteredRoleIDs)
    }
}

export default RoleArchive;
