import RoleArchive from './role-archive'

const archives = document.querySelectorAll('.role-archive');

for(let archive of archives) {
    new RoleArchive(archive);
}
