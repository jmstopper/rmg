<?php

add_filter('stratum/render/assets/components/role-archive', function (array $args): array {

    if (is_post_type_archive('rmg-role')) {
        $args['sidebar_header_heading'] = get_field('role_archive_header_heading', 'option');
        $args['sidebar_header_content'] = get_field('role_archive_header_content', 'option');
        $args['sidebar_footer_heading'] = get_field('role_archive_footer_heading', 'option');
        $args['sidebar_footer_content'] = get_field('role_archive_footer_content', 'option');
    }

    $args['roles'] = array_map(function ($item) {

        if (in_array(get_post_type($item), ['rmg-role'])) {

            $logo = get_field('job_logo', $item);
            $map = get_field('map', $item->ID);

            $lat = !empty($map['markers'][0]['lat']) ? $map['markers'][0]['lat'] : '';
            $lng = !empty($map['markers'][0]['lng']) ? $map['markers'][0]['lng'] : '';

            if (!empty($logo['id'])) {
                $logo = wp_get_attachment_image($logo['id'], 'large');
            } else {
                $logo = '';
            }

            // -----------------------------------------------------------------
            // Setup the base role
            // -----------------------------------------------------------------
            $role = [
                'link' => !empty(get_field('external_url', $item->ID)) ? get_field('external_url', $item->ID) : get_the_permalink($item->ID),
                'id' => $item->ID,
                'heading' => get_the_title($item->ID),
                'excerpt' => get_the_excerpt($item->ID),
                'date' => get_the_date('', $item->ID),
                'text_location' => get_field('location', $item->ID),
                'location' => rmgGetJobTerms($item->ID, 'rmg-location'),
                'vacancy_type' => rmgGetJobTerms($item->ID, 'rmg-vacancy-type'),
                'job_category' => rmgGetJobTerms($item->ID, 'rmg-job-category'),
                'salary' => get_field('salary', $item->ID),
                'brand_company' => get_field('brand_company', $item->ID),
                'logo' => $logo,
                'lat' => $lat,
                'lng' => $lng,
                'latlnghash' => !empty($lat) ? md5($lat . $lng) : '',
                'pin' => \Stratum\imageURL('pin-' . theme() . '.svg'),
            ];

            return $role;
        }
    }, $args['roles']);

    return $args;
});

function rmgGetJobTerms($postID, $taxonomy)
{
    $terms = get_the_terms($postID, $taxonomy);

    if (empty($terms)) {
        return [];
    }

    return array_map(function ($term) {
        $return = [
            'id' => $term->term_id,
            'slug' => $term->slug,
            'name' => $term->name,
            'count' => $term->count,
            'short_term_and_flexible' => get_field('short_term_and_flexible', $term),
        ];

        return $return;
    }, $terms);
}
