
<?php
// -----------------------------------------------------------------------------
// Images
// -----------------------------------------------------------------------------
?>
<link rel="preload" href="<?= \Stratum\imageURL('logo.svg'); ?>" as="image">
<link rel="preload" href="<?= \Stratum\imageURL('logo-inline.svg'); ?>" as="image">
<link rel="preload" href="<?= \Stratum\imageURL('symbol.svg'); ?>" as="image">
<link rel="preload" href="<?= \Stratum\imageURL('tel.svg'); ?>" as="image">
<link rel="preload" href="<?= \Stratum\imageURL('arrow.svg'); ?>" as="image">
<?php if (is_front_page()) { ?>
    <link rel="preload" href="<?= \Stratum\Paths\assetURL('components/logo/logo.png'); ?>" as="image">
    <link rel="preload" href="<?= \Stratum\imageURL('overlay-complex-bottom.svg'); ?>" as="image" media="(min-width: 768px)">
    <link rel="preload" href="<?= \Stratum\imageURL('overlay-complex-middle.svg'); ?>" as="image" media="(min-width: 768px)">
    <link rel="preload" href="<?= \Stratum\imageURL('overlay-complex-top.svg'); ?>" as="image" media="(min-width: 768px)">
<?php } ?>

<?php
// -----------------------------------------------------------------------------
// Fonts
// -----------------------------------------------------------------------------
?>
<link rel="preload" href="<?= \Stratum\Paths\assetURL('fonts/argentc-bold.woff2'); ?>" as="font" type="font/woff2" crossorigin>
<link rel="preload" href="<?= \Stratum\Paths\assetURL('fonts/gotham-book.woff2'); ?>" as="font" type="font/woff2" crossorigin>
<link rel="preload" href="<?= \Stratum\Paths\assetURL('fonts/gotham-medium.woff2'); ?>" as="font" type="font/woff2" crossorigin>
<link rel="preload" href="<?= \Stratum\Paths\assetURL('fonts/gotham-bold.woff2'); ?>" as="font" type="font/woff2" crossorigin>
<link rel="preload" href="<?= \Stratum\Paths\assetURL('fonts/gotham-bolditalic.woff2'); ?>" as="font" type="font/woff2" crossorigin>
<link rel="preload" href="<?= \Stratum\imageURL('overlay-mobile.svg'); ?>" as="image" media="(max-width: 767px)">

<?php
// -----------------------------------------------------------------------------
// Javascript
// -----------------------------------------------------------------------------
?>
<link rel="preload" href="<?= \Stratum\Paths\assetURL('scripts/scripts.js', true); ?>" as="script">
<?php if (is_front_page()) { ?>
    <link rel="preload" href="<?= \Stratum\Paths\assetURL('components/logo/data.json', false); ?>" as="script">
<?php } ?>

