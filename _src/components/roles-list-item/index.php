<?php

$classes = ['roles-list-item'];

if (!empty($args['vacancy_type'][0]['short_term_and_flexible'])) {
    $classes[] = 'is-flexible';
} else {
    $classes[] = 'is-fixed';
}

?><div class="<?= implode(' ', $classes); ?>" data-role-id="<?= esc_attr($args['id']); ?>">
    <h3 class="roles-list-item__heading">
        <a href="<?= esc_url($args['link']); ?>">
            <?= $args['heading']; ?>
        </a>
    </h3>

    <div class="roles-list-item__date">
        <?= $args['date']; ?>
    </div>

    <?php if (!empty($args['text_location'])) { ?>
        <div class="roles-list-item__location">
            <?= $args['text_location']; ?>
        </div>
    <?php } ?>

    <?php if (!empty($args['salary'])) { ?>
        <div class="roles-list-item__salary">
            <?= $args['salary']; ?>
        </div>
    <?php } ?>

    <div class="roles-list-item__type">
        <span>Position type: </span>
        <?php if (count($args['vacancy_type']) === 0) {
            _e('Unknown', 'stratum');
        } elseif (count($args['vacancy_type']) === 1) {
            echo $args['vacancy_type'][0]['name'];
        } else {
            _e('Multiple', 'stratum');
        } ?>
    </div>

    <div class="roles-list-item__category">
        <span>Category: </span>
        <?php if (count($args['job_category']) === 0) {
            _e('Unknown', 'stratum');
        } elseif (count($args['job_category']) === 1) {
            echo $args['job_category'][0]['name'];
        } else {
            _e('Multiple', 'stratum');
        } ?>
    </div>

    <?php if (!empty($args['logo'])) { ?>
        <div class="roles-list-item__logo img-fit is-contain">
            <?= $args['logo']; ?>
        </div>
    <?php } ?>
</div>
