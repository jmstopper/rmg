<?php

add_filter('stratum/render/assets/components/twocolumns', function (array $args): array {

    $args = wp_parse_args($args, [
        'id' => '',
    ]);

    if ($args['show_contact_details'] === true) {
        $args['left_content'] = \Stratum\render('assets/components/iconlist', [
            'items' => get_field('contact_details', 'option')
        ]) . $args['left_content'];
    }

    return $args;
});
