<div class="twocolumns" id="<?= esc_attr($args['id']); ?>">
    <div class="twocolumns__row">
        <div class="twocolumns__col twocolumns__col--left">
            <?php if (!empty($args['left_heading'])) { ?>
                <div class="twocolumns__left-heading">
                    <?= $args['left_heading']; ?>
                </div>
            <?php } ?>

            <?= $args['left_content']; ?>
        </div>
        <div class="twocolumns__col twocolumns__col--right">
            <?= $args['right_content']; ?>
        </div>
    </div>
</div>
