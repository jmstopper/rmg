<div class="overlappedcards" id="<?= esc_attr($args['id']); ?>">
    <?php if (!empty($args['heading'])) { ?>
        <h2 class="overlappedcards__heading">
            <?= $args['heading']; ?>
        </h2>
    <?php } ?>

    <?php if (!empty($args['meta'])) { ?>
        <div class="overlappedcards__meta">
            <?= \Stratum\render('assets/components/tag', $args['meta']); ?>
        </div>
    <?php } ?>

    <div class="overlappedcards__items">
        <?php foreach ($args['items'] as $key => $item) {
            echo \Stratum\render('assets/components/card', $item);
        } ?>
    </div>

    <?php if (!empty($args['footer'])) { ?>
        <div class="overlappedcards__footer">
            <?= $args['footer']; ?>
        </div>
    <?php } ?>
</div>
