class RoleList {
    constructor(element) {
        this.element = element;
        this.toggles = element.querySelectorAll('.js-role-list-toggle');
        this.filters = element.querySelector('.roles-list__filters');

        for (const toggle of this.toggles) {
            toggle.addEventListener('click', () => {
                toggle.classList.toggle('active');
                this.filters.classList.toggle('active');
            });
        }
    }
}

export default RoleList;
