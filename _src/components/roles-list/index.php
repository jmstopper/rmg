<?php

$filters = [
    'location' => [
        'label' => __('Location', 'rmg'),
        'placeholder' => __('All locations', 'rmg'),
        'options' => array_map(function ($term) {
            return [
                'name' => $term->name,
                'slug' => $term->slug,
            ];
        }, get_terms([
            'taxonomy' => 'rmg-location',
            'hide_empty' => false,
        ])),
    ],
    'vacancy_type' => [
        'label' => __('Vacancy Type', 'rmg'),
        'placeholder' => __('All vacancy types', 'rmg'),
        'options' => array_map(function ($term) {
            return [
                'name' => $term->name,
                'slug' => $term->slug,
            ];
        }, get_terms([
            'taxonomy' => 'rmg-vacancy-type',
            'hide_empty' => false,
        ])),
    ],
    'job_category' => [
        'label' => __('Job Category', 'rmg'),
        'placeholder' => __('All job categories', 'rmg'),
        'options' => array_map(function ($term) {
            return [
                'name' => $term->name,
                'slug' => $term->slug,
            ];
        }, get_terms([
            'taxonomy' => 'rmg-job-category',
            'hide_empty' => false,
        ])),
    ]
];

$showFlexible = count(array_filter($args['roles'], function ($role) {
    return !empty($role['vacancy_type'][0]['short_term_and_flexible']);
})) > 0;

?><div class="roles-list">
    <div class="roles-list__sidebar">
        <div class="roles-list__sidebar-header">
            <h1 class="roles-list__sidebar-header-heading">
                <?= $args['sidebar_header_heading']; ?>
            </h1>

            <div class="roles-list__sidebar-header-content">
                <?= $args['sidebar_header_content']; ?>
            </div>
        </div>

        <?php if (!empty($args['roles'])) { ?>

            <button class="roles-list__toggle js-role-list-toggle"><?php _e('Filter', 'stratum') ?></button>

            <form class="roles-list__filters">

                <div class="roles-list__filter is-text">
                    <label>
                        <span class="screen-reader-text"><?php _e('Keyword', 'stratum'); ?></span>
                        <input class="js-keyword" type="search" placeholder="<?php esc_attr_e('Enter keyword', 'stratum'); ?>">
                    </label>
                </div>

                <?php if (!empty($filters)) {
                    foreach ($filters as $key => $filter) {
                        if (!empty($filter['options'])) { ?>
                            <div class="roles-list__filter is-select">
                                <label>
                                    <span class="screen-reader-text"><?= $filter['label']; ?></span>
                                    <select class="js-filter" data-slug="<?= esc_attr($key); ?>">
                                        <option value="" selected><?= $filter['placeholder']; ?></option>
                                        <?php foreach ($filter['options'] as $key => $option) { ?>
                                            <option value="<?= esc_attr($option['slug']); ?>">
                                                <?= $option['name']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </label>
                            </div>
                <?php }
                    }
                } ?>

                <?php /*
                <div class="roles-list__filter">
                    <button type="reset"><?php _e('Reset', 'stratum'); ?></button>
                </div>
                */ ?>

                <p class="roles-list__form-footer">
                    <button type="button" class="js-role-list-toggle button"><?php _e('Hide filter', 'stratum') ?></button>
                </p>
            </form>

        <?php } ?>

        <?php if (!empty($args['sidebar_footer_content'])) { ?>
            <div class="roles-list__sidebar-footer">
                <div class="roles-list__sidebar-footer-heading">
                    <?= $args['sidebar_footer_heading']; ?>
                </div>
                <?= $args['sidebar_footer_content']; ?>
            </div>
        <?php } ?>
    </div>


    <div class="roles-list__roles">
        <?php if ($showFlexible) { ?>
            <h2 class="h5 is-flexible-heading roles-list__roles-heading"><?= __('Short term and flexible roles', 'rmg'); ?></h2>

            <?php foreach ($args['roles'] as $key => $role) {
                if (!empty($role['vacancy_type'][0]['short_term_and_flexible'])) {
                    echo \Stratum\render('assets/components/roles-list-item', $role);
                }
            } ?>
        <?php } ?>

        <h2 class="h5 is-fixed-heading roles-list__roles-heading"><?= __('Permanent full and part time roles', 'rmg'); ?></h2>
        <?php foreach ($args['roles'] as $key => $role) {
            if (empty($role['vacancy_type'][0]['short_term_and_flexible'])) {
                echo \Stratum\render('assets/components/roles-list-item', $role);
            }
        } ?>

        <p class="roles-list__none"><?php _e('Check back for upcoming opportunities'); ?></p>
    </div>

    <div class="roles-list__footer">
        <div class="roles-list__footer-heading">
            <?= $args['sidebar_footer_heading']; ?>
        </div>
        <?= $args['sidebar_footer_content']; ?>
    </div>
</div>