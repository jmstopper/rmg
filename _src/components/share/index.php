<?php

echo \Stratum\render('assets/components/socialicons', [
    'heading' => __('Share this', 'stratum'),
    'networks' => [
        [
            'network' => 'facebook',
            'url' => 'https://www.facebook.com/sharer/sharer.php?u=' . esc_attr($args['url'])
        ],
        [
            'network' => 'twitter',
            'url' => 'https://twitter.com/home?status=' . esc_attr($args['url'])
        ],
        [
            'network' => 'linkedin',
            'url' => 'https://www.linkedin.com/shareArticle?mini=true&url=' . esc_attr($args['url'])
        ]
    ]
]);
