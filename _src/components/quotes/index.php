<?php if (!empty($args['quotes'])) { ?>
    <div class="quotes alignfull" id="<?= esc_attr($args['id']); ?>">

        <div class="quotes__quotes">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php foreach ($args['quotes'] as $key => $item) { ?>
                        <div class="swiper-slide">
                            <div class="quotes__item">
                                <?php
                                ob_start();

                                ?><blockquote class="quotes__quote">
                                    <?= $item['content']; ?>

                                    <cite>
                                        <span class="quotes__author">
                                            <?= $item['author']; ?>
                                        </span>
                                        <span class="quotes__role">
                                            <?= $item['position']; ?>
                                        </span>
                                    </cite>
                                </blockquote><?php

                                $content = ob_get_clean();

                                echo \Stratum\render('assets/components/flag', [
                                    'content' => $content,
                                    'media' => $item['media'],
                                    'meta' => $args['meta'],
                                ]);
                                                ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <?php if (count($args['quotes']) > 1) { ?>
                <div class="swiper-pagination"></div>

                <div class="swiper-buttons">
                    <div class="swiper-button swiper-button-prev">
                        <span class="screen-reader-text">
                            <?php _e('Previous slide') ?>
                        </span>
                    </div>
                    <div class="swiper-button swiper-button-next">
                        <span class="screen-reader-text">
                            <?php _e('Next slide') ?>
                        </span>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>
