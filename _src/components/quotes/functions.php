<?php

add_filter('stratum/render/assets/components/quotes', function (array $args): array {

    $args = wp_parse_args($args, [
        'id' => '',
    ]);

    if (!empty($args['tag'])) {
        $args['meta'] = $args['tag'];
        unset($args['tag']);
    }

    if (!empty($args['quotes'])) {
        $args['quotes'] = array_map(function ($quote) {

            $image = '';

            if (has_post_thumbnail($quote->ID)) {
                $image = get_the_post_thumbnail($quote->ID, 'large');
            }

            return [
                'content' => apply_filters('the_content', get_the_content(null, false, $quote->ID)),
                'author' => get_the_title($quote->ID),
                'position' => get_field('position', $quote->ID),
                'media' => $image
            ];
        }, $args['quotes']);
    }

    return $args;
});
