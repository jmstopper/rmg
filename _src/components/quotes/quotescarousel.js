import Swiper from 'swiper';

class QuotesCarousel {
    constructor(wrapper) {
        this.wrapper = wrapper;
        this.carouselEl = this.wrapper.querySelector('.quotes__quotes .swiper-container');

        if (this.carouselEl.querySelectorAll('.swiper-slide').length > 1) {
            this.init();
        }
    }

    init() {
        this.controlCarousel = new Swiper(this.carouselEl, {
            slidesPerView: 1,
            loop: true,
            pagination: {
                el: this.wrapper.querySelector('.swiper-pagination'),
            },
            navigation: {
                nextEl: this.wrapper.querySelector('.swiper-button-next'),
                prevEl: this.wrapper.querySelector('.swiper-button-prev'),
            },
        });
    }
}

export default QuotesCarousel;
