import QuotesCarousel from './quotescarousel';

const sliders = document.querySelectorAll('.quotes');

for(const slider of sliders) {
    new QuotesCarousel(slider);
}
