<?php

add_filter('stratum/render', function ($args) {

    // Check if the shape argument is set and if it has not been processed already
    if (!empty($args['shape']) && \Stratum\startsWith($args['shape'], 'url') !== true) {

        $imageURL = \Stratum\imageURL($args['shape']);
        $theme = theme();

        if($theme === 'blue'){
            $imageURL = substr($imageURL, 0, -4) . '-blue.svg';
        }

        if($theme === 'purple'){
            $imageURL = substr($imageURL, 0, -4) . '-purple.svg';
        }

        $args['shape'] = 'url(' . $imageURL . ')';
    }

    return $args;
});
