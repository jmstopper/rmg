import * as basicScroll from 'basicscroll';

const shapes = document.querySelectorAll('.shape');

for (const shape of shapes) {

    basicScroll.create({
        elem: shape,
        direct: true, // add the css variable to the element explicitly
        from: 'top-bottom',
        to: 'bottom-top',
        props: {
            '--translateY': {
                from: '-30%',
                to: '10%',
            },
            '--scale': {
                from: 0.9,
                to: 1.1
            }
        }
    }).start();
}

