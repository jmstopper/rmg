<?php
$args = wp_parse_args($args, [
    'id' => '',
]);
?>
<div class="widecard" id="<?= esc_attr($args['id']); ?>">
    <?= \Stratum\render('assets/components/card', $args); ?>
</div>
