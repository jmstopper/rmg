<aside class="article-footer">
    <?php if (!empty($args['content'])) { ?>
        <h2 class="article-footer__heading">
            <?= $args['heading']; ?>
        </h2>
    <?php } ?>
    <?php if (!empty($args['content'])) { ?>
        <div class="article-footer__content">
            <?= $args['content']; ?>
        </div>
    <?php } ?>
</aside>
