import DualCarousel from './dualcarousel';

const sliders = document.querySelectorAll('.dualcarousel');

for(const slider of sliders) {
    new DualCarousel(slider);
}
