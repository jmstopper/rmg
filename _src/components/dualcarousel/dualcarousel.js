import Swiper from 'swiper';

class DualCarousel {
    constructor(wrapper) {
        this.wrapper = wrapper;
        this.controlCarouselEl = this.wrapper.querySelector('.dualcarousel__control-carousel .swiper-container');
        this.contentCarouselEl = this.wrapper.querySelector('.dualcarousel__content-carousel .swiper-container');

        this.init();
    }

    init() {
        this.contentCarousel = new Swiper(this.contentCarouselEl, {
            slidesPerView: 1,
            allowTouchMove: false,
            autoHeight: true,
        });

        this.controlCarousel = new Swiper(this.controlCarouselEl, {
            slidesPerView: 3,
            // freeMode: true,
            loop: true,
            slideToClickedSlide: true,
            centeredSlides: true,
            navigation: {
                nextEl: this.wrapper.querySelector('.swiper-button-next'),
                prevEl: this.wrapper.querySelector('.swiper-button-prev'),
            },
            breakpoints: {
                768: {
                    slidesPerView: 4
                }
            }
        });

        this.controlCarousel.on('slideChange', () => {
            this.contentCarousel.slideTo(this.controlCarousel.realIndex);
        });
    }
}

export default DualCarousel;
