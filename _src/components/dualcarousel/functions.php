<?php

add_filter('stratum/render/assets/components/dualcarousel', function (array $args): array {

    $args = wp_parse_args($args, [
        'id' => '',
    ]);

    $imageSize = 'large';

    if (!empty($args['tag'])) {
        $args['meta'] = $args['tag'];
        unset($args['tag']);
    }

    if (!empty($args['work'])) {
        $args['items'] = $args['work'];
        unset($args['work']);
    }

    if (!empty($args['team'])) {
        $args['items'] = $args['team'];
        unset($args['team']);
    }

    $args['items'] = array_map(function ($item) use($imageSize) {

        $return = [];

        $quote = false;

        if ($item instanceof WP_Post) {
            $return['heading'] = get_the_title($item);
            $return['content'] = apply_filters('the_content', get_the_content(null, false, $item));
            $return['media'] = get_the_post_thumbnail($item, $imageSize);

            if (get_post_type($item) === 'rmg-team') {
                $return['meta'] = get_field('position', $item);
            }

            if (get_post_type($item) === 'rmg-work') {
                $return['content'] = get_field('summary', $item);
                $quote = get_field('quote', $item);
                $logo = get_field('logo', $item);

                if (!empty($logo)) {
                    $return['icon'] = wp_get_attachment_image($logo['id'], $imageSize);
                }
            }
        } else {
            $return['heading'] = $item['heading'];
            $return['content'] = $item['content'];
            $quote = $item['quote'];

            if ($item['type'] === 'icon') {
                $return['icon'] = wp_get_attachment_image($item['image']['id'], $imageSize);
            } else {
                $return['media'] = wp_get_attachment_image($item['image']['id'], $imageSize);
            }
        }

        // Check if we need to add a quote
        if (!empty($quote)) {
            $return['quote'] = [
                'content' => apply_filters('the_content', get_the_content(null, false, $quote)),
                'author' => get_the_title($quote),
                'position' => get_field('position', $quote),
            ];
        }

        return $return;
    }, $args['items']);

    return $args;
});
