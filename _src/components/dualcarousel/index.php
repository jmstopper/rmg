<div class="dualcarousel alignfull shape" style="--shapeurl:<?= esc_attr($args['shape']); ?>" id="<?= esc_attr($args['id']); ?>">

    <div class="dualcarousel__header">
        <?php if (!empty($args['heading'])) { ?>
            <h2 class="dualcarousel__heading">
                <?= $args['heading']; ?>
            </h2>
        <?php } ?>

        <?php if (!empty($args['meta'])) { ?>
            <div class="dualcarousel__meta">
                <?= \Stratum\render('assets/components/tag', $args['meta']); ?>
            </div>
        <?php } ?>
    </div>

    <div class="dualcarousel__control-carousel">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach ($args['items'] as $key => $item) {
                    $classes = ['dualcarousel__control-carousel-item'];

                    if (!empty($item['icon'])) {
                        $classes[] = 'dualcarousel__control-carousel-item--icon';
                    } else {
                        $classes[] = 'img-fit';
                    }

                    ?>
                    <div class="swiper-slide" data-key="<?= esc_attr($key); ?>">
                        <div class="<?= esc_attr(implode(' ', $classes)); ?>">
                            <?php if (!empty($item['icon'])) {
                                echo $item['icon'];
                            } elseif ($item['media']) {
                                echo $item['media'];
                            } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="swiper-button swiper-button-prev">
            <span class="screen-reader-text">
                <?php _e('Previous slide') ?>
            </span>
        </div>
        <div class="swiper-button swiper-button-next">
            <span class="screen-reader-text">
                <?php _e('Next slide') ?>
            </span>
        </div>
    </div>

    <div class="dualcarousel__content-carousel">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach ($args['items'] as $key => $item) { ?>
                    <div class="swiper-slide">
                        <div class="dualcarousel__item">
                            <div class="dualcarousel__item-wrap">
                                <div class="dualcarousel__item-inner">
                                    <?php if (!empty($item['heading']) && $item['heading'] !== '') { ?>
                                        <div class="dualcarousel__item-heading">
                                            <?= $item['heading']; ?>
                                        </div>
                                    <?php } ?>

                                    <?php if (!empty($item['meta']) && $item['meta'] !== '') { ?>
                                        <div class="dualcarousel__item-meta">
                                            <?= $item['meta']; ?>
                                        </div>
                                    <?php } ?>

                                    <?php if (!empty($item['content']) && $item['content'] !== '') { ?>
                                        <div class="dualcarousel__item-content">
                                            <?= $item['content']; ?>
                                        </div>
                                    <?php } ?>
                                </div>

                                <?php if (!empty($item['quote'])) { ?>
                                    <div class="dualcarousel__item-quote">
                                        <?= $item['quote']['content']; ?>

                                        <?php if (!empty($item['quote']['author'])) { ?>
                                            <p>
                                                <span class="dualcarousel__item-quote-author">
                                                    <?= $item['quote']['author']; ?>
                                                </span>
                                                <?php if (!empty($item['quote']['position'])) { ?>
                                                    <span class="dualcarousel__item-quote-position">
                                                        <?= $item['quote']['position']; ?>
                                                    </span>
                                                <?php } ?>
                                            </p>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
