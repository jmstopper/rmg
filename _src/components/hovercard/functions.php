<?php

add_filter('stratum/render/assets/components/hovercard', function (array $args): array {
    if (!empty($args['image']['id'])) {
        $args['media'] = wp_get_attachment_image($args['image']['id'], 'large');
        unset($args['image']);
    }

    if (!empty($args['icon']['id'])) {
        $args['icon'] = wp_get_attachment_image($args['icon']['id'], 'post-thumbnail');
    }

    if (!empty($args['link'])) {
        $args['link_element'] = '<a href="' . esc_url($args['link']['url']) . '">' . $args['link']['title'] . '</a>';
    }

    return $args;
});
