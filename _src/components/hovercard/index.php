<div class="hover-card">
    <div class="hover-card__inner">
        <?php if (!empty($args['icon'])) { ?>
            <div class="hover-card__icon img-fit" aria-hidden="true">
                <?= $args['icon']; ?>
            </div>
        <?php } ?>

        <?php if (!empty($args['heading'])) { ?>
            <h3 class="hover-card__heading">
                <?php if (!empty($args['link'])) { ?>
                    <a href="<?= esc_attr($args['link']['url']); ?>">
                <?php } ?>

                <?= $args['heading']; ?>

                <?php if (!empty($args['link'])) { ?>
                    </a>
                <?php } ?>

            </h3>
        <?php } ?>

        <?php if (!empty($args['content'])) { ?>
            <div class="hover-card__content">
                <?= $args['content']; ?>
            </div>
        <?php } ?>
        <?php if (!empty($args['link_element'])) { ?>
            <p class="hover-card__link">
                <?= $args['link_element']; ?>
            </p>
        <?php } ?>
    </div>

    <?php if (!empty($args['media'])) { ?>
        <div class="hover-card__media img-fit">
            <?= $args['media']; ?>
        </div>
    <?php } ?>
</div>
