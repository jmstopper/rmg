<div class="overview">
    <?php if (!empty($args['heading'])) { ?>
        <h2 class="overview__heading">
            <?= $args['heading']; ?>
        </h2>
    <?php } ?>

    <div class="overview__items">
        <?php foreach ($args['items'] as $key => $item) { ?>
            <div class="overview__item" style="--iconurl:url(<?= $item['icon']; ?>)">
                <div class="overview__item-heading">
                    <?= $item['heading']; ?>:
                </div>
                <div class="overview__item-content">
                    <?= $item['content']; ?>
                </div>
            </div>
        <?php } ?>
    </div>

    <?php if ($args['content'] !== '') { ?>
        <div class="overview__content">
            <?= $args['content']; ?>
        </div>
    <?php } ?>
</div>
