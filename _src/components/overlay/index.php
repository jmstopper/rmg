<?php

$classes = ['overlay'];

if (!empty($args['media'])) {
    if (!empty($args['complex']) && $args['complex'] === true) {
        $classes[] = 'overlay--complex';
    } else {
        $classes[] = 'overlay--media';
    }
}

?><div class="<?= esc_attr(implode(' ', $classes)); ?>">
    <div class="overlay__outer-wrap">
        <div class="overlay__wrap">
            <div class="overlay__inner">
                <?php if (!empty($args['heading'])) { ?>
                    <h1 class="overlay__heading">
                        <?= $args['heading']; ?>
                    </h1>
                <?php } ?>

                <?php if (!empty($args['meta'])) { ?>
                    <div class="overlay__meta">
                        <?= \Stratum\render('assets/components/tag', $args['meta']); ?>
                    </div>
                <?php } ?>

                <?php if (!empty($args['content'])) { ?>
                    <div class="overlay__content">
                        <?= $args['content']; ?>
                    </div>
                <?php } ?>
            </div>

            <?php if (!empty($args['media'])) { ?>
                <div class="overlay__media img-fit">
                    <?= $args['media']; ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
