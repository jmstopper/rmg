<footer class="footer">

    <?= \Stratum\render('assets/components/iconcards', [
        'cards' => get_field('footer_cards', 'option')
    ]); ?>

    <div class="footer__row">
        <nav class="footer__navigation footer__col footer__content--menu" role="navigation" aria-label="<?php esc_attr_e('Footer navigation', 'stratum'); ?>">
            <?php wp_nav_menu([
                'theme_location'  => 'footer',
                'depth'           => 1,
                'container'       => '',
                'container_class' => '',
                'menu_class'      => 'footer__menu',
                'fallback_cb'     => false,
            ]); ?>
        </nav>

        <div class="footer__col footer__content footer__content--content">
            <a class="footer__logo" href="<?= esc_url(get_home_url()); ?>">
                <?= \Stratum\image('logo-inline.svg', [
                    'alt' => get_bloginfo('name'),
                ]); ?>
            </a>

            <?php the_field('footer_content', 'option'); ?>
        </div>
    </div>
    <div class="footer__row">
        <div class="footer__col footer__content footer__content--copyright">
            <?php the_field('footer_copyright', 'option') ?>
        </div>
        <div class="footer__col footer__content--social">
            <?= \Stratum\render('assets/components/socialicons', [
                'networks' => get_field('social_networks', 'option')
            ]); ?>
        </div>
    </div>
</footer>
