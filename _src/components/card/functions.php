<?php

add_filter('stratum/render/assets/components/card', function ($args): array {

    // -------------------------------------------------------------------------
    // If the item is a wordpress post
    // -------------------------------------------------------------------------
    if ($args instanceof WP_Post) {
        $myCard = [
            'heading' => get_the_title($args->ID),
            'meta' => [],
            'content' => '',
            'link' => get_the_permalink($args->ID),
            'media' => get_the_post_thumbnail($args->ID, 'large')
        ];

        if (get_post_type($args) === 'rmg-work') {
            $myCard['content'] = get_field('summary', $args->ID);
        }

        $myCard['content'] .= '<p class="c-card__link"><a href="' . esc_url(get_the_permalink($args->ID)) . '">' . __('Learn more', 'stratum') . '</a></p>';

        $terms = get_the_terms($args, 'category');

        if (!empty($terms) && !is_wp_error($terms)) {
            $myCard['meta'][] = '<a href="' . get_term_link($terms[0]) . '">' . $terms[0]->name . '</a>';
        }

        $myCard['meta'][] = get_the_date(get_option('date_format'), $args);

        $myCard['meta'] = implode(' | ', $myCard['meta']);

        $args = $myCard;
    } else {
        if (!empty($args['link'])) {
            $button = '<p class="c-card__link"><a href="' . esc_url($args['link']['url']) . '">' . $args['link']['title'] . '</a></p>';
            $args['content'] .= $button;
            $args['link'] = $args['link']['url'];
        }
    }

    // -------------------------------------------------------------------------
    // If the item has an image argument
    // -------------------------------------------------------------------------
    if (!empty($args['image']['id'])) {
        $args['media'] = wp_get_attachment_image($args['image']['id'], 'large');
        unset($args['image']);
    }

    return $args;
});
