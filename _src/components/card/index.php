<?php

$classes = ['c-card'];

if (!empty($args['media'])) {
    $classes[] = 'has-media';
}

?><div class="<?= esc_attr(implode(' ', $classes)); ?>">
    <div class="c-card__inner">
        <?php if (!empty($args['heading'])) { ?>
            <div class="c-card__heading">
                <?php if (!empty($args['link'])) { ?>
                    <a href="<?= esc_attr($args['link']); ?>">
                <?php } ?>

                <?= $args['heading']; ?>

                <?php if (!empty($args['link'])) { ?>
                    </a>
                <?php } ?>
            </div>
        <?php } ?>

        <?php if (!empty($args['meta'])) { ?>
            <div class="c-card__meta">
                <?= $args['meta']; ?>
            </div>
        <?php } ?>

        <?php if (!empty($args['content'])) { ?>
            <div class="c-card__content">
                <?= $args['content']; ?>
            </div>
        <?php } ?>
    </div>

    <?php if (!empty($args['tag'])) { ?>
        <div class="c-card__tag">
            <?= \Stratum\render('assets/components/tag', $args['tag']); ?>
        </div>
    <?php } ?>

    <?php if (!empty($args['media'])) {
        $element = !empty($args['link']) ? 'a' : 'div';
        $elementClose = $element;

        if ($element === 'a') {
            $element .= ' href="'.esc_attr($args['link']).'"';
        }

        ?>

        <<?= $element; ?> class="c-card__media img-fit">
            <?= $args['media']; ?>

            <?php if (!empty($args['pill'])) { ?>
                <div class="c-card__pill">
                    <?= $args['pill']; ?>
                </div>
            <?php } ?>
        </<?= $elementClose; ?>>
    <?php } ?>
</div>
