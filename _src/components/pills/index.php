<ul class="pills" data-heading="<?= $args['heading']; ?>">
    <?php foreach ($args['items'] as $key => $pill) {
        $class = ['pill'];

        if ($pill['selected'] === true) {
            $class[] = 'active';
        }

        ?>
        <li>
            <a
                class="<?= esc_attr(implode(' ', $class)); ?>"
                href="<?= esc_url($pill['url']); ?>"
                <?php if (!empty($pill['slug'])) {
                    ?>data-term="<?= esc_attr($pill['slug']) ?>"<?php
                } ?>
                >
                <?= $pill['name']; ?>
            </a>
        </li>
    <?php } ?>
</ul>
