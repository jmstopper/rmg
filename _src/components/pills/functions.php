<?php

add_filter('stratum/render/assets/components/pills', function (array $args): array {

    $queriedObject = get_queried_object();
    $currentTermID = !empty($queriedObject->term_id) ? $queriedObject->term_id : null;

    $args['items'] = array_map(function ($item) use($currentTermID) {
        return [
            'name' => $item->name,
            'slug' => $item->slug,
            'url' => get_term_link($item),
            'selected' => $item->term_id === $currentTermID,
        ];
    }, $args['items']);

    if (!empty($args['root'])) {
        array_unshift($args['items'], $args['root']);
        unset($args['root']);
    }

    return $args;
});
