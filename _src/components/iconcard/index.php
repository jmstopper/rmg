<?php

$classes[] = 'iconcard';

if ($args['icon'] != '') {
    $classes[] = 'has-icon';
}

?><div class="<?= esc_attr(implode(' ', $classes)); ?>" style="--iconurl:<?= esc_attr($args['icon']); ?>;">
    <?php if (!empty($args['heading'])) { ?>
        <h3 class="iconcard__heading">
            <?= $args['heading']; ?>
        </h3>
    <?php } ?>

    <?php if (!empty($args['content'])) { ?>
        <div class="iconcard__content">
            <?= $args['content']; ?>
        </div>
    <?php } ?>
</div>
