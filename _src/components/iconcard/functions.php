<?php

add_filter('stratum/render/assets/components/iconcard', function (array $args): array {

    if(theme() === 'blue' && !empty($args['icon_blue'])){
        $args['icon'] = 'url(' . $args['icon_blue']['sizes']['icon'] . ')';
    }elseif(theme() === 'purple' && !empty($args['icon_purple'])){
        $args['icon'] = 'url(' . $args['icon_purple']['sizes']['icon'] . ')';
    }else if (!empty($args['icon'])) {
        $args['icon'] = 'url(' . $args['icon']['sizes']['icon'] . ')';
    }

    if (!empty($args['link'])) {
        $button = '<a class="button" href="' . esc_url($args['link']['url']) . '">' . $args['link']['title'] . '</a>';
        $args['content'] .= apply_filters('the_content', $button);
    }

    return $args;
});
