<?php

add_filter('stratum/render/assets/components/wysiwyg', function (array $args): array {

    $args = wp_parse_args($args, [
        'id' => '',
    ]);

    if (!empty($args['tag'])) {
        $args['meta'] = $args['tag'];
        unset($args['tag']);
    }

    if (!empty($args['background']) && $args['background'] === 'none') {
        unset($args['background']);
    }

    if (!empty($args['link']) && is_array($args['link'])) {
        $args['content'] .= '<p class="wysiwyg__button"><a class="button" href="' . esc_url($args['link']['url']) . '">' . $args['link']['title'] . '</a></p>';
    }

    return $args;
});
