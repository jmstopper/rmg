<?php
// Merge default arguments
$args = array_replace([
    '_key' => 0,
    'background' => '',
    'heading' => '',
    'content' => '',
    'altstyle' => false,
    'infographic' => false
], $args);

$classes = ['wysiwyg'];

// Add required arguments
if ($args['background'] !== '') {
    $classes[] = 'wysiwyg--background';
    $classes[] = 'wysiwyg--' . $args['background'];
}

if ($args['altstyle'] === true) {
    $classes[] = 'wysiwyg--altstyle';
}

?><div class="<?= esc_attr(implode(' ', $classes)); ?>" id="<?= esc_attr($args['id']); ?>">
    <?php if (!empty($args['heading'])) { ?>
        <h2 class="wysiwyg__heading">
            <?= $args['heading']; ?>
        </h2>
    <?php } ?>

    <?php if (!empty($args['meta'])) { ?>
        <div class="wysiwyg__meta">
            <?= \Stratum\render('assets/components/tag', $args['meta']); ?>
        </div>
    <?php } ?>

    <?php if ($args['content'] !== '') { ?>
        <div class="wysiwyg__content">
            <?= $args['content']; ?>
        </div>
    <?php } ?>
</div>
