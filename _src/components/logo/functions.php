<?php

add_filter('stratum/render/assets/components/logo', function (array $args): array {
    return wp_parse_args( $args, [
        'override' => false
    ]);
});
