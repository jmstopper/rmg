import lottie from 'lottie-web';

const wp = window.wp_vars;

lottie.loadAnimation({
    container: document.querySelector('.logo__animation'),
    loop: false,
    autoplay: true,
    path: wp.themePath + '/assets/components/logo/data.json'
});

