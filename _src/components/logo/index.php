<?php

$customLogo = false;

$logo = \Stratum\image('logo-' . theme() . '.svg', [
    'class' => 'logo__static',
    'alt' => get_bloginfo('name'),
]);

$symbol = \Stratum\image('symbol-' . theme() . '.svg', [
    'class' => 'logo__symbol'
]);

if($args['override'] === true) {
    $globalLogo = get_field('logo', 'option');
    $pageLogo = get_field('logo');

    $globalIcon = get_field('logo_icon', 'option');
    $icon = get_field('logo_icon');

    if(!empty($globalLogo['id'])) {
        $customLogo = true;
        $logo = wp_get_attachment_image( $globalLogo['id'], 'small', false, [
            'class' => 'logo__static logo__static--custom',
            'alt' => get_bloginfo('name'),
        ]);
    }

    if(!empty($pageLogo['id'])) {
        $customLogo = true;
        $logo = wp_get_attachment_image( $pageLogo['id'], 'small', false, [
            'class' => 'logo__static logo__static--custom',
            'alt' => get_bloginfo('name'),
        ]);
    }

    if(!empty($globalIcon['id'])){
        $symbol = wp_get_attachment_image( $globalIcon['id'], 'small', false, [
            'class' => 'logo__symbol logo__symbol--custom',
            'alt' => '',
        ]);
    }

    if(!empty($icon['id'])){
        $symbol = wp_get_attachment_image( $icon['id'], 'small', false, [
            'class' => 'logo__symbol logo__symbol--custom',
            'alt' => '',
        ]);
    }
}

?><div class="logo">
    <?php if($customLogo === true){

        echo $logo;
        echo $symbol;

    }else { ?>
        <div class="logo__animation"></div>

        <?= $logo ?>
        <?= $symbol; ?>

        <?= \Stratum\image('logo-inline.svg', [
            'class' => 'logo__inline'
        ]); ?>
    <?php } ?>
</div>
