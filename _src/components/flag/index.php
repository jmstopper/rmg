<?php

$classes = ['flag'];

if (!empty($args['reverse']) && $args['reverse'] === true) {
    $classes[] = 'flag--reverse';
}

if (!empty($args['media_heading'])) {
    $classes[] = 'flag--media-up';
}

?><div class="<?= esc_attr(implode(' ', $classes)); ?>" id="<?= esc_attr($args['id']); ?>">

    <div class="flag__inner">
        <?php if (!empty($args['heading'])) { ?>
            <h2 class="flag__heading">
                <?= $args['heading']; ?>
            </h2>
        <?php } ?>

        <?php if (!empty($args['meta'])) { ?>
            <div class="flag__meta">
                <?= \Stratum\render('assets/components/tag', $args['meta']); ?>
            </div>
        <?php } ?>

        <?php if (!empty($args['content'])) { ?>
            <div class="flag__content">
                <?= $args['content']; ?>
            </div>
        <?php } ?>

        <?php if (!empty($args['link'])) { ?>
            <div class="flag__link">
                <?= $args['link']; ?>
            </div>
        <?php } ?>
    </div>

    <?php if (!empty($args['media'])) { ?>
        <div class="flag__media">
            <div class="flag__media-media img-fit shape" style="--shapeurl:<?= esc_attr($args['shape']); ?>">
                <?= $args['media']; ?>
            </div>

            <?php if (!empty($args['media_heading'])) { ?>
                <div class="flag__media-heading">
                    <?= $args['media_heading']; ?>
                </div>
            <?php } ?>

            <?php if (!empty($args['media_content'])) { ?>
                <div class="flag__media-content">
                    <?= $args['media_content']; ?>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>
