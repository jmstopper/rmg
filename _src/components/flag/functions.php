<?php

add_filter('stratum/render/assets/components/flag', function (array $args): array {

    $args = wp_parse_args($args, [
        'id' => '',
    ]);

    if (!empty($args['tag'])) {
        $args['meta'] = $args['tag'];
        unset($args['tag']);
    }

    if (!empty($args['image']['id'])) {
        $args['media'] = wp_get_attachment_image($args['image']['id'], 'large');
        unset($args['image']);
    }

    if (!empty($args['link'])) {
        $button = '<a class="button" href="' . esc_url($args['link']['url']) . '">' . $args['link']['title'] . '</a>';
        $args['link'] = $button;
    } else {
        unset($args['link']);
    }

    return $args;
});
