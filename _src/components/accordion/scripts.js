import getSiblings from '../../scripts/helpers/getSiblings';

const items = document.querySelectorAll('details');

for (const item of items) {
    item.addEventListener('click', () => {
        const siblings = getSiblings(item);

        for (const sibling of siblings) {
            sibling.removeAttribute('open');
        }
    });
}
