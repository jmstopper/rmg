<?php

$classes = ['accordion'];

if ($args['reverse'] === true) {
    $classes[] = 'accordion--reverse';
}

?><div class="<?= esc_attr(implode(' ', $classes)); ?>">
    <div class="accordion__main">

        <?php if (!empty($args['heading'])) { ?>
            <h2 class="accordion__heading">
                <?= $args['heading']; ?>
            </h2>
        <?php } ?>

        <?php if (!empty($args['meta'])) { ?>
            <div class="accordion__meta">
                <?= \Stratum\render('assets/components/tag', $args['meta']); ?>
            </div>
        <?php } ?>

        <div class="accordion__items">
            <?php foreach ($args['items'] as $key => $item) { ?>
                <details class="accordion__item" <?php if ($key === 0) {
                    ?>open<?php
                                                 } ?>>
                    <summary class="accordion__item-heading">
                        <?= $item['heading'] ?>
                    </summary>
                    <div class="accordion__item-content">
                        <?= $item['content']; ?>
                    </div>
                </details>
            <?php } ?>
        </div>
    </div>

    <?php if (!empty($args['media'])) { ?>
        <div class="accordion__media img-fit shape" style="--shapeurl:<?= esc_attr($args['shape']); ?>">
            <?= $args['media']; ?>
        </div>
    <?php } ?>
</div>
