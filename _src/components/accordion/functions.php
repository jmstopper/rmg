<?php

add_filter('stratum/render/assets/components/accordion', function (array $args): array {

    if (!empty($args['tag'])) {
        $args['meta'] = $args['tag'];
        unset($args['tag']);
    }

    if (!empty($args['image']['id'])) {
        $args['media'] = wp_get_attachment_image($args['image']['id'], 'large');
        unset($args['image']);
    }

    return $args;
});
