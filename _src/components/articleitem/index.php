<?php

$classes = ['article-item'];

if (!empty($args['overlap']) && $args['overlap'] === true) {
    $classes[] = 'article-item--overlap';
}

if (!empty($args['share']) && $args['share'] === true) {
    $classes[] = 'article-item--share';
}

if (empty($args['author'])) {
    $classes[] = 'article-item--no-author';
}

?><div class="<?= esc_attr(implode(' ', $classes)); ?>">
    <div class="article-item__header">

        <?php if (!empty($args['heading'])) { ?>
            <h1 class="article-item__heading">
                <?= $args['heading']; ?>
            </h1>
        <?php } ?>

        <?php if (!empty($args['meta'])) { ?>
            <div class="article-item__meta">
                <?= $args['meta']; ?>
            </div>
        <?php } ?>

        <?php if (!empty($args['author'])) { ?>
            <div class="article-item__author">
                <div class="article-item__author-inner">
                    <?php if (!empty($args['author']['name'])) { ?>
                        <div class="article-item__author-name">
                            <?= $args['author']['name']; ?>
                        </div>
                    <?php } ?>

                    <?php if (!empty($args['author']['content'])) { ?>
                        <div class="article-item__author-content">
                            <?= $args['author']['content']; ?>
                        </div>
                    <?php } ?>
                </div>
                <?php if (!empty($args['author']['media'])) { ?>
                    <div class="article-item__author-media img-fit">
                        <?= $args['author']['media']; ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>

        <?php if ($args['share'] === true) {
            echo \Stratum\render('assets/components/share', [
                'url' => get_the_permalink()
            ]);
        } ?>
    </div>

    <?php if (!empty($args['tag'])) { ?>
        <div class="article-item__tag">
            <?= \Stratum\render('assets/components/tag', $args['tag']); ?>
        </div>
    <?php } ?>

    <?php if (!empty($args['media'])) { ?>
        <div class="article-item__media">
            <div class="article-item__media-inner img-fit">
                <?= $args['media']; ?>
            </div>
        </div>
    <?php } ?>

    <?php if (!empty($args['content'])) { ?>
        <div class="article-item__content">
            <?= $args['content']; ?>
        </div>
    <?php } ?>
</div>
