<?php

add_filter('stratum/render/assets/components/timeline', function (array $args): array {

    if (!empty($args['tag'])) {
        $args['meta'] = $args['tag'];
        unset($args['tag']);
    }

    $args['items'] = array_map(function ($item) {
        $item['icon'] = wp_get_attachment_image($item['image']['id'], 'medium');

        unset($item['image']);

        return $item;
    }, $args['items']);

    return $args;
});
