<?php

$args = wp_parse_args($args, [
    'heading' => '',
    'meta' => '',
    'items' => [],
    'numbers' => false,
]);

$classes = ['timeline'];

if ($args['numbers'] === true) {
    $classes[] = 'timeline--numbers';
}

if (!empty($args['items'])) { ?>
    <div class="<?= esc_attr(implode(' ', $classes)); ?>" id="<?= esc_attr($args['id']); ?>">
        <?php if (!empty($args['heading'])) { ?>
            <h2 class="timeline__heading">
                <?= $args['heading']; ?>
            </h2>
        <?php } ?>

        <?php if (!empty($args['meta'])) { ?>
            <div class="timeline__meta">
                <?= \Stratum\render('assets/components/tag', $args['meta']); ?>
            </div>
        <?php } ?>

        <ol class="timeline__items">
            <?php foreach ($args['items'] as $key => $item) {
                $counter = sprintf("%02d", $key + 1);

                ?><li class="timeline__item">
                    <h3 class="timeline__item-heading">
                        <span>
                            <?= $item['heading']; ?>
                        </span>
                    </h3>

                    <div class="timeline__item-content">
                        <?= $item['content']; ?>
                    </div>

                    <div class="timeline__item-icon img-fit">
                        <?= $item['icon']; ?>
                    </div>

                    <div class="timeline__item-counter">
                        <?= $counter; ?>
                    </div>
                </li>
            <?php } ?>
        </ol>
    </div>
<?php }
