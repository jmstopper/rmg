const body = document.querySelector('body');


if (body.classList.contains('blog') || body.classList.contains('post-type-archive-rmg-work')) {

    const cards = document.querySelectorAll('.cards__col');
    const selector = document.querySelector('.selecturl select');

    selector.addEventListener('change', (event) => {
        event.preventDefault();
        const term = selector.options[selector.selectedIndex].dataset.term;

        for (const card of cards) {
            if (term === 'all' || card.classList.contains(term)) {
                card.style.display = 'block';
            } else {
                card.style.display = 'none';
            }
        }
    });
} else {
    const selectors = document.querySelectorAll('.selecturl select');

    for (const selector of selectors) {
        selector.addEventListener('change', () => {
            const url = selector.options[selector.selectedIndex].value;

            window.location.href = url;
        });
    }
}
