<div class="selecturl">
    <select class="selecturl__select">
        <?php foreach ($args['items'] as $key => $item) { ?>
            <option
                value="<?= esc_url($item['url']); ?>"
                <?php if ($item['selected'] === true) { ?>
                    selected
                <?php } ?>
                <?php if (!empty($item['slug'])) { ?>
                    data-term="<?= $item['slug']; ?>"
                <?php } ?>
            ><?= esc_html($item['name']); ?></option>
        <?php } ?>
    </select>
</div>
