import ie10 from './helpers/ie10';
import ie11 from './helpers/ie11';
import '../components/_components';

if (ie11() === true || ie10() === true) {
    document.querySelector('html').classList.add('legacy-browser');
}
