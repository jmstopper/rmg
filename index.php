<?php

get_header();

if (have_posts()) {
    $cardsContent = '';
    $myPosts = []; // A placeholder to put all our posts in
    $content = ''; // A placeholder to put all our content in

    // -------------------------------------------------------------------------
    // Build the header
    // -------------------------------------------------------------------------
    $content .= \Stratum\render('assets/components/overlay', [
        'heading' => get_field('posts_archive_heading', 'option'),
        'content' => get_field('posts_archive_content', 'option'),
    ]);

    // -------------------------------------------------------------------------
    // Add any filtering components
    // -------------------------------------------------------------------------
    $cardsContent .= \Stratum\render('assets/components/selecturl', [
        'root' => [
            'name' => __('All Insights', 'stratum'),
            'slug' => 'all',
            'url' => get_the_permalink(get_option('page_for_posts')),
            'selected' => is_home()
        ],
        'items' => get_terms([
            'taxonomy' => 'category',
        ])
    ]);

    // -------------------------------------------------------------------------
    // Loop the posts and store them
    // -------------------------------------------------------------------------
    while (have_posts()) {
        the_post();
        $myPosts[] = $post;
    }

    // -------------------------------------------------------------------------
    // Add the posts to our content output
    // -------------------------------------------------------------------------
    $content .= \Stratum\render('assets/components/cards', [
        'content' => $cardsContent,
        'cards' => $myPosts,
    ]);

    // -------------------------------------------------------------------------
    // Output everything
    // -------------------------------------------------------------------------
    echo \Stratum\render('assets/components/main', $content);

    echo \Stratum\render('partials/wordpress/posts-pagination');
} else {
    echo \Stratum\render(
        'assets/components/main',
        \Stratum\render('partials/wordpress/content-none')
    );
}

get_footer();
