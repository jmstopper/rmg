<?php

get_header();

if (have_posts()) {
    while (have_posts()) {
        the_post();
        $taxonomy = get_post_type() === 'rmg-work' ? 'rmg-work-category' : 'category';

        $term = '';
        $terms = get_the_terms(get_the_ID(), $taxonomy);

        if (!empty($terms)) {
            $term = '<a href="' . get_term_link($terms[0]) . '">' . $terms[0]->name . '</a>';
        }

        $articleItemArgs = [
            'tag' => $term,
            'meta' => '',
            'heading' => get_the_title(),
            'media' => get_the_post_thumbnail(get_the_id(), 'large'),
            'content' => apply_filters('the_content', get_the_content()),
            'share' => false,
            'overlap' => false,
        ];

        if (get_post_type() === 'post') {
            $articleItemArgs['overlap'] = true;
            $articleItemArgs['share'] = true;
            $articleItemArgs['meta'] = get_the_date();

            $author = get_field('author');

            if ($author) {
                $articleItemArgs['author'] = [
                    'name' => get_the_title($author),
                    'content' => wpautop(get_field('position', $author)),
                    'media' => \Stratum\image('silhouette.png')
                ];

                if (has_post_thumbnail($author)) {
                    $articleItemArgs['author']['media'] = get_the_post_thumbnail($author, 'thumbnail');
                }
            }
        }

        $mainContent = \Stratum\render(
            'assets/components/article',
            \Stratum\render('assets/components/articleitem', $articleItemArgs)
        );

        if (get_post_type() === 'rmg-work') {
            $next = get_next_post();

            if ($next === '') {
                $next = get_posts([
                    'post_type' => 'rmg-work',
                    'posts_per_page' => 1,
                    'orderby' => 'date',
                    'order' => 'ASC',
                ]);

                $next = !empty($next) ? $next[0] : '';
            }

            if (!empty($next)) {
                $nextcontent = get_field('summary', $next);
                $nextcontent .= '<p><a href="' . get_the_permalink($next) . '">' . __('Learn more', 'stratum') . '</a></p>';

                $mainContent .= \Stratum\render('assets/components/articlefooter', [
                    'heading' => __('Next case study', 'stratum'),
                    'content' => $nextcontent
                ]);
            }
        }

        if (get_post_type() === 'post') {
            $items = [];

            // Change this to not just be the next and previous, but two latest
            // posts from the same category.

            $next = get_next_post();
            $prev = get_previous_post();

            if ($next !== '') {
                $items[] = $next;
            }

            if ($prev !== '') {
                $items[] = $prev;
            }

            $mainContent .= \Stratum\render('assets/components/cards', [
                'tag' => __('Next', 'stratum'),
                'items' => $items,
                'footer' => '<p><a class="button" href="' . get_the_permalink(get_option('page_for_posts')) . '">' . __('View all', 'stratum') . '</a></p>'
            ]);
        }

        echo \Stratum\render('assets/components/main', $mainContent);
    }
} else {
    echo \Stratum\render(
        'assets/components/main',
        \Stratum\render('partials/wordpress/content-none')
    );
}

get_footer();
