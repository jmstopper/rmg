<?php
// Import Stratum
require 'stratum/stratum.php';

// Content type registration
require 'functions/pt-post.php';
require 'functions/pt-work.php';
require 'functions/pt-role.php';
require 'functions/pt-team.php';
require 'functions/pt-author.php';
require 'functions/pt-quote.php';
require 'functions/tax-vacancy-type.php';
require 'functions/tax-category.php';
require 'functions/tax-location.php';
require 'functions/tax-job-category.php';
require 'functions/tax-work-category.php';
require 'functions/acf-shape.php';
require 'functions/theme.php';
require 'functions/gravity-forms.php';

// Everything else
require 'functions/media-to-video.php';

add_filter('jpeg_quality', function(){
    return 100;
});

add_filter('big_image_size_threshold', function ($threshold) {
    return 4000;
});

add_filter('body_class', function($classes){

    $theme = theme();

    if($theme !== ''){
        $classes[] = 'theme--' . $theme;
    }

    return $classes;
});

// Make the gravity form button display on all tinymce editors
add_filter('gform_display_add_form_button', '__return_true');
