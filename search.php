<?php

get_header();

if (have_posts()) {
    $cardsContent = '';
    $myPosts = []; // A placeholder to put all our posts in
    $content = ''; // A placeholder to put all our content in

    // -------------------------------------------------------------------------
    // Build the header
    // -------------------------------------------------------------------------
    $content .= \Stratum\render('assets/components/overlay', [
        'heading' => __('Search')
    ]);

    // -------------------------------------------------------------------------
    // Loop the posts and store them
    // -------------------------------------------------------------------------
    while (have_posts()) {
        the_post();
        $myPosts[] = $post;
    }

    // -------------------------------------------------------------------------
    // Add the posts to our content output
    // -------------------------------------------------------------------------
    $content .= \Stratum\render('assets/components/cards', [
        'content' => $cardsContent,
        'cards' => $myPosts,
    ]);

    // -------------------------------------------------------------------------
    // Output everything
    // -------------------------------------------------------------------------
    echo \Stratum\render('assets/components/main', $content);
    echo \Stratum\render('partials/wordpress/posts-pagination');
} else {
    echo \Stratum\render(
        'assets/components/main',
        \Stratum\render('partials/wordpress/content-none')
    );
}

get_footer();
