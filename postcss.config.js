module.exports = {
    plugins: [
        require('cssnano')({
            preset: ['default', {
                discardComments: {
                    removeAll: true
                },
                discardUnused: true,
                mergeIdents: true,
                reduceIdents: true,
            }]
        }),
    ],
};
