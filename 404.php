<?php

get_header();

echo \Stratum\render(
    'assets/components/main',
    \Stratum\render('partials/wordpress/content-none')
);

get_footer();
