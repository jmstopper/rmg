<?php

get_header();

global $post;

// A placeholder to put all our content in
$myPosts = [];

while (have_posts()) {
    the_post();
    $myPosts[] = $post;
}

echo \Stratum\render(
    'assets/components/main',
    \Stratum\render('assets/components/role-archive', [
        'map' => true,
        'roles' => $myPosts
    ])
);

get_footer();
