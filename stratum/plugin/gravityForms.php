<?php

namespace Stratum\Plugin;

class GravityForms
{
    public static function init(): void
    {
        // Stop the scrolling on page reload/ajax submit
        add_filter('gform_confirmation_anchor', '__return_false');

        // Progress should be at 0 on first page of form
        add_filter('gform_progressbar_start_at_zero', '__return_true');

        // Move gravity forms inline js and css to footer
        // Does not apply when using ajax
        // https://docs.gravityforms.com/gform_init_scripts_footer/
        add_filter('gform_init_scripts_footer', '__return_true');

        add_filter('gform_next_button', [__CLASS__, 'input_to_button'], 10, 2);
        add_filter('gform_previous_button', [__CLASS__, 'input_to_button'], 10, 2);
        add_filter('gform_submit_button', [__CLASS__, 'input_to_button'], 10, 2);


        // Everything else
        add_action('gform_enqueue_scripts', [__CLASS__, 'dequeueGFStylesheets'], 11);
        add_filter('gform_pre_render', [__CLASS__, 'addFieldAsClass']);
        add_filter('gformGetFormFilter', [__CLASS__, 'gformGetFormFilter'], 10, 1);
    }

    /**
     * Gravity forms loves to add lots of rubbish CSS, lets disable it as we
     * are dealing with it in the theme
     * @return void
     */
    public static function dequeueGFStylesheets(): void
    {
        // Make sure were not on a gravity admin page
        if (is_admin() && isset($_GET['gf_page'])) {
            return;
        }

        wp_dequeue_style('gforms_reset_css');
        wp_dequeue_style('gforms_datepicker_css');
        wp_dequeue_style('gforms_formsmain_css');
        wp_dequeue_style('gforms_ready_class_css');
        wp_dequeue_style('gforms_browsers_css');
    }

    /**
     * Every now and then, we need to know what type of field we are working
     * with, this will add the field type as a class to the output. Adds Field
     * types as a class to the container
     * @param array $form
     * @return array
     */
    public static function addFieldAsClass(array $form): array
    {
        foreach ($form['fields'] as $f => $field) {
            $form['fields'][$f]['cssClass'] .= ' gfield-type-' . $field['type'] . ' ';
        }

        return $form;
    }

    /**
     * This hack searches the output for <style></style> and removes it. We do this
     * because gravity loves to write super specific styling that we cant over-right
     * @param string $form_string
     * @return string
     */
    public static function gformGetFormFilter(string $formString): string
    {
        return preg_replace(
            '/<\s*style.+?<\s*\/\s*style.*?>/si',
            ' ',
            $formString
        );
    }

    /**
     * Turn input buttons to buttons
     */
    public static function input_to_button($button, $form)
    {
        $dom = new \DOMDocument();
        $dom->loadHTML($button);
        $input = $dom->getElementsByTagName('input')->item(0);
        $new_button = $dom->createElement('button');
        $new_button->appendChild($dom->createTextNode($input->getAttribute('value')));
        $input->removeAttribute('value');

        foreach ($input->attributes as $attribute) {
            $new_button->setAttribute($attribute->name, $attribute->value);
        }

        $input->parentNode->replaceChild($new_button, $input);

        return $dom->saveHtml($new_button);
    }
}
