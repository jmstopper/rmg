<?php

namespace Stratum\Plugin;

class ACF
{
    public static function init(): void
    {
        add_action('acf/init', [__CLASS__, 'google']);
        add_action('acf/init', [__CLASS__, 'optionPages']);
        add_filter('the_content', [__CLASS__, 'flexFilter'], 1);
        add_filter('acf/settings/remove_wp_meta_box', '__return_false');


    }

    public static function google(): void
    {
        // Register a google maps api key
        if (function_exists('acf_update_setting') && defined('STM_GOOGLE_API_KEY') && STRATUM_GOOGLE_API_KEY != '') {
            acf_update_setting('google_api_key', STRATUM_GOOGLE_API_KEY);
        }
    }

    /**
     * Provide ACF with a Google maps API key if it has been configured
     * @return void
     */
    public static function optionPages(): void
    {
        /**
         * Add an options page to the theme for global settings.
         * Runs without needing a hook
         */
        if (function_exists('acf_add_options_page')) {
            // Global Option pages
            if (defined('STRATUM_ACF_OPTIONS_PAGES') && is_array(STRATUM_ACF_OPTIONS_PAGES)) {
                acf_add_options_page();

                foreach (STRATUM_ACF_OPTIONS_PAGES as $key => $page) {
                    acf_add_options_sub_page($page);
                }
            }

            // Post type option pages
            if (defined('STRATUM_ACF_POST_OPTIONS') && is_array(STRATUM_ACF_POST_OPTIONS)) {
                foreach (STRATUM_ACF_POST_OPTIONS as $key => $page) {
                    $parent = 'edit.php';

                    if ($page['type'] !== 'post') {
                        $parent = $parent . '?post_type=' . $page['type'];
                    }

                    acf_add_options_sub_page([
                        'title'      => esc_html($page['name']),
                        'parent'     => $parent,
                        'capability' => 'manage_options'
                    ]);
                }
            }
        }
    }

    /**
     * Determines if we are on a flexible content template, and if so, will
     * build out the content and return it. This is a method of having plugins
     * still be able to apply filters with the_content. Most notable this is
     * needed when restricting access to pages
     * @param string $content
     * @return string
     */
    public static function flexFilter(string $content): string
    {
        if (self::isFlexibleContentTemplate(get_the_id()) && !post_password_required()) {
            $content = \Stratum\render(
                'partials/components/flexible-content',
                self::getFlexibleContent()
            );

            // Disable autop to address extra tags around whitespace
            // Inside the if statement, we generate the content to be returned
            // before we remove the wpautop filter so we don't get p tags for
            // whitespace. If we performed the function call on the return
            // statement, we would have to remove the filter, then add it back
            // in when generating each of the content blocks.
            remove_filter('the_content', 'wpautop');
        }

        return $content;
    }

    /**
     * Determine if the template being loaded is build with flexible content
     * @param int|null $post_id
     * @return boolean
     */
    public static function isFlexibleContentTemplate($post_id = null)
    {
        if ($post_id === null) {
            $post_id = get_the_id();
        }

        return get_page_template_slug($post_id) === 'templates/flexible-content.php';
    }

    public static function getFlexibleContent($field = 'flexible_content', $context = null)
    {
        if ($context === null) {
            $context = get_the_id();
        }

        return get_field($field, $context);
    }
}
