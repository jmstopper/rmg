<?php

// Get the configuration for this project
require_once 'config/general.php';
require_once 'config/colors.php';
require_once 'config/font-sizes.php';
require_once 'config/menus.php';
require_once 'config/images.php';
require_once 'config/sidebars.php';
require_once 'config/post-formats.php';
require_once 'config/mimes.php';
require_once 'config/acf.php';

// core
require_once 'core/autoload.php';
require_once 'core/image.php';
require_once 'core/svg.php';
require_once 'core/paths.php';
require_once 'core/helpers.php';
require_once 'core/render-helpers.php';
require_once 'core/render.php';

// WordPress optimisations
\Stratum\WordPress\Admin::init();
\Stratum\WordPress\Gutenberg::init();
\Stratum\WordPress\Head::init();
\Stratum\WordPress\Images::init();
\Stratum\WordPress\LazyLoad::init();
\Stratum\WordPress\Menus::init();
\Stratum\WordPress\Scripts::init();
\Stratum\WordPress\Search::init();
\Stratum\WordPress\Sidebars::init();
\Stratum\WordPress\ThemeSupport::init();
\Stratum\WordPress\ServiceWorker::init();

// Components needs to be instantiated as it keep track of acf save locations
new \Stratum\WordPress\Components;

// Plugin optimisations
\Stratum\Plugin\ACF::init();
\Stratum\Plugin\GravityForms::init();
\Stratum\Plugin\YoastSEO::init();
