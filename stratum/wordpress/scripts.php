<?php
namespace Stratum\WordPress;

class Scripts
{
    public static function init()
    {
        // Add the styles and scripts
        add_action('wp_enqueue_scripts', [__CLASS__, 'register'], 15);
        add_action('wp_enqueue_scripts', [__CLASS__, 'enqueue'], 16);

        // Remove the version strings
        add_filter('style_loader_src', [__CLASS__, 'removeVersionString'], 9999, 2);
        add_filter('script_loader_src', [__CLASS__, 'removeVersionString'], 9999, 2);

        add_action('enqueue_block_editor_assets', [__CLASS__, 'gutenbergStyles']);
    }

    public static function gutenbergStyles():void
    {
        wp_enqueue_style('stratum-styles-gutenberg', \Stratum\Paths\assetURL('styles/gutenberg.css', true), []);
    }

    /**
     * Registers styles and scripts required by our theme
     * @return void
     */
    public static function register()
    {
        // Placeholder for the dependencies of our main JS file
        $scriptDependencies = [];

        /*
        * If we are in wp-admin, load the default stylesheet, otherwise load
        * up our custom stylesheet. This probs has to change with Gutenberg on
        * the horizon
        */
        wp_register_style(
            'stratum-style-core',
            is_admin() ?
                get_stylesheet_uri() :
                \Stratum\Paths\assetURL('styles/styles.css', true)
        );

        if (defined('STRATUM_GOOGLE_API_KEY') &&  STRATUM_GOOGLE_API_KEY !== '') {
            wp_register_script('stratum-script-map', esc_url('https://maps.googleapis.com/maps/api/js?key=' . STRATUM_GOOGLE_API_KEY), [], '', true);
        }

        if (defined('STRATUM_REQUIRE_JQUERY') && STRATUM_REQUIRE_JQUERY === true) {
            $scriptDependencies[] = 'jquery-core';
        }

        if (self::gravityForms()) {
            $scriptDependencies[] = 'jquery-core';
        }

        // Register the main script file
        wp_register_script(
            'stratum-script-core',
            \Stratum\Paths\assetURL('scripts/scripts.js', true),
            $scriptDependencies,
            '',
            true
        );
    }

    /**
     * Enqueue scripts that we have registered
     * @return void
     */
    public static function enqueue()
    {
        if (STRATUM_AJAX === true) {
            // You should probably add a nonce here for each ajax action taken
            // https://codex.wordpress.org/Function_Reference/wp_create_nonce
            wp_localize_script('stratum-script-core', 'wp_vars', [
                'ajaxURL'  => esc_url(admin_url('admin-ajax.php')),
                'homeURL'  => esc_url(home_url()),
                'themePath' => get_template_directory_uri()
            ]);
        }

        if (wp_script_is('stratum-script-map', 'registered') && STRATUM_MAPS === true) {
            wp_enqueue_script('stratum-script-map');
        }

        if (wp_style_is('stratum-style-core', 'registered')) {
            wp_enqueue_style('stratum-style-core');
        }

        if (wp_script_is('stratum-script-core', 'registered')) {
            wp_enqueue_script('stratum-script-core');
        }
    }

    public static function removeVersionString($src, $handle): string
    {
        if (strpos($src, 'stratum/assets') !== false) {
            $src = remove_query_arg('ver', $src);
        }

        return $src;
    }

    private static function gravityForms()
    {
        global $post;

        $content = get_the_content(null, false, $post);
        $content .= get_field('overlay_content', $post);

        if (strpos($content, 'gform_wrapper') !== false) {
            return true;
        }

        return false;
    }
}
