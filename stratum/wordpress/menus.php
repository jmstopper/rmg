<?php
namespace Stratum\WordPress;

class Menus
{
    public static function init()
    {
        add_action('after_setup_theme', [__CLASS__, 'registerNavMenus']);
    }

    /**
     * Register menus for use in WordPress
     *
     * Loops over each element defined in STRATUM_MENUS and registers a menu for it.
     * We use this to shortcut the overhead each time.
     * @return void
     */
    public static function registerNavMenus()
    {
        global $STRATUM_MENUS;
        if (!empty($STRATUM_MENUS)) {
            register_nav_menus($STRATUM_MENUS);
        }
    }
}
