<?php
namespace Stratum\WordPress;

class Sidebars
{
    public static function init()
    {
        add_action('widgets_init', [__CLASS__, 'registerSidebars']);
    }

    /**
     * Register sidebars for use in WordPress
     *
     * Loops over each element defined in STRATUM_SIDEBARS and registers a sidebar for
     * it. We use this to shortcut the overhead each time.
     * @return void
     */
    public static function registerSidebars()
    {
        global $STRATUM_SIDEBARS;

        if (is_array($STRATUM_SIDEBARS)) {
            foreach ($STRATUM_SIDEBARS as $key => $sidebar) {
                register_sidebar($sidebar);
            }
        }
    }
}
