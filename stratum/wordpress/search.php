<?php
namespace Stratum\WordPress;

class Search
{
    public static function init()
    {
        add_action('template_redirect', [__CLASS__, 'redirectSingle']);
    }

    /**
     * Redirect to the single page if search results only return one entry
     * @return type
     */
    public static function redirectSingle()
    {
        if (is_search()) {
            global $wp_query;

            if ($wp_query->post_count == 1 && $wp_query->max_num_pages == 1) {
                wp_redirect(get_permalink($wp_query->posts[0]->ID));
                exit;
            }
        }
    }
}
