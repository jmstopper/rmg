<?php

namespace Stratum\WordPress;

class ServiceWorker
{
    public static function init()
    {
        add_action('admin_init', [__CLASS__, 'copyServiceWorker']);
    }

    public static function copyServiceWorker()
    {
        $source = \Stratum\Paths\assetPath('general/serviceworker.js');
        $dest = get_home_path() . 'serviceworker.js';

        copy($source, $dest);
    }
}
