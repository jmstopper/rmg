<?php

namespace Stratum\WordPress;

class Components
{

    // The current block being saved
    var $blockPath = '';

    public function __construct()
    {
        self::init();

        // Hook in to the field update action
        add_action('acf/update_field_group', [$this, 'updateFieldGroup'], 1);
    }

    public static function init(): void
    {
        add_action('init', [__CLASS__, 'loadFunctions']);
        add_action('acf/init', [__CLASS__, 'loadBlocks']);
        add_action('acf/settings/load_json', [__CLASS__, 'updateLoadPath']);
    }

    public static function loadBlocks(): void
    {
        $blocks = glob(\Stratum\Paths\assetPath('components/*/acf.php'));

        foreach ($blocks as $key => $block) {
            require_once $block;
        }
    }

    public static function loadFunctions(): void
    {
        $blocks = glob(\Stratum\Paths\assetPath('components/*/functions.php'));

        foreach ($blocks as $key => $block) {
            require_once $block;
        }
    }

    public static function updateLoadPath(array $paths): array
    {
        $blocks = glob(\Stratum\Paths\assetPath('components/*'));

        return array_merge($paths, $blocks);
    }

    public function updateFieldGroup($group): void
    {
        // Check if the field group is assigned to a block, and if the block is
        // in the _src/components folder
        if (!empty($group['location'][0][0]['param']) && $group['location'][0][0]['param'] === 'block') {
            // Pull the actual block name
            // This strip dashes for some reason
            $block = str_replace('acf/', '', $group['location'][0][0]['value']);

            // Build a path to the block source code
            $blockPath = \Stratum\Paths\themePath('_src/components/' . $block);

            // Check the path exists
            if (is_dir($blockPath)) {
                // Set the block we are currently saving
                $this->blockPath = $blockPath;

                // Hook in to the save json method
                add_action('acf/settings/save_json', [$this, 'updateSavePath'], 9999);
            }
        }
    }

    public function updateSavePath(string $path): string
    {
        // change the path the block is stored at
        return $this->blockPath;
    }
}
