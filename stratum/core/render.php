<?php

namespace Stratum;

/**
 * Retrieve a partial from the theme and pass arguments to it.
 *
 * Like https://developer.wordpress.org/reference/functions/get_template_part/
 * but allows for arguments to be passed in the form or an array. Each partial
 * defines its own array arguments so view each partial to see what you
 * can/should pass
 * @param string $path
 * @param array $args
 * @return void
 */
function render(string $path, $args = ''): string
{
    // A component specific render filter
    $args = apply_filters('stratum/render/' . $path, $args);

    // A generic render filter
    $args = apply_filters('stratum/render', $args, $path);

    // Push it to the stack
    \Stratum\Render\push($path, $args);

    // Turn it in to a real file path
    $path = \Stratum\Paths\resolve($path);

    // stop the output from happening
    ob_start();

    // import the partial
    require($path);

    // remove the partial
    \Stratum\Render\pop();

    // return it
    return ob_get_clean();
}
