<?php

namespace Stratum;

/**
 * Return an image from the assets folder
 * @param string $name
 * @param array $args
 * @param bool $output
 * @return string
 */
function image(string $name, array $args = []): string
{
    $args = wp_parse_args($args, [
        'alt' => '',
        'class' => '',
    ]);

    // Drop the src tag in
    $args['src'] = \Stratum\imageURL($name);

    // Remove the class attribute if it is empty
    if ($args['class'] === '') {
        unset($args['class']);
    }

    // If the alt tag is empty lets make it a presentation element
    if ($args['alt'] === '') {
        $args['role'] = 'presentation';
    }

    // Build a url string that is scaped
    foreach ($args as $key => $value) {
        $args[$key] = esc_attr($key) . '="'
            // If we are handling a URL, we need to use the esc_url function
            . ($key === 'src' ?
                esc_url($value)
                : esc_attr($value))
            . '"';
    }

    // Return the image tag
    return '<img ' . implode(' ', $args) . '>';
}

/**
 * Build the URL for the image
 * @param string $name
 * @return string
 */
function imageURL(string $name): string
{
    return \Stratum\Paths\assetURL('images/' . $name);
}
