<?php
/**
 * Define the menus you would like available
 */
$STRATUM_MENUS = [
    'header' => __('Header', 'stratum'),
    'mobile' => __('Mobile', 'stratum'),
    'topheader' => __('Top Header', 'stratum'),
    'footer' => __('Footer', 'stratum'),
];
