<?php
/**
 * WordPress core options
 */

// Disable emojis
const STRATUM_EMOJI = false;

/**
 * Theme options
 */
const STRATUM_AJAX = true;
const STRATUM_CONTENT_WIDTH = 1170;
const STRATUM_FONTS_CSS = false;
const STRATUM_FONTS_JS = false;
const STRATUM_GOOGLE_API_KEY = '';
const STRATUM_REQUIRE_JQUERY = false;

// Controls whether or not to load the legacy js file which adds support for ie11
const STRATUM_LEGACY_BROWSERS = false;
