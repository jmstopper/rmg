<?php

$STRATUM_COLORS = [
    [
        'name' => __('Black', 'stratum'),
        'slug' => 'black',
        'color' => '#000',
    ],
    [
        'name' => __('White', 'stratum'),
        'slug' => 'white',
        'color' => '#fff',
    ],
];
