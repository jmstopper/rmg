<?php

get_header();

if (have_posts()) {
    $showFeatured = true;
    $myPosts = []; // A placeholder to put all our posts in
    $content = ''; // A placeholder to put all our content in
    $cardsContent = '';

    // -------------------------------------------------------------------------
    // Build the header
    // -------------------------------------------------------------------------
    $content .= \Stratum\render('assets/components/overlay', [
        'heading' => get_field('work_archive_heading', 'option'),
        'content' => get_field('work_archive_content', 'option'),
    ]);

    // -------------------------------------------------------------------------
    // Are we on the Work post type archive
    // -------------------------------------------------------------------------
    if ($featuredItem = get_field('work_featured_item', 'option')) {
        if (is_tax('rmg-work-category')) {
            $showFeatured = false;

            $queriedTerm = get_queried_object();
            $terms = get_the_terms($featuredItem, 'rmg-work-category');

            if (!empty($terms)) {
                foreach ($terms as $term) {
                    if ($term->term_id === $queriedTerm->term_id) {
                        $showFeatured = true;
                        break;
                    }
                }
            }
        }

        if ($showFeatured === true) {
            $myPosts[] = $featuredItem;
        }
    }

    // -------------------------------------------------------------------------
    // Loop the posts and store them
    // -------------------------------------------------------------------------
    while (have_posts()) {
        the_post();
        global $post;
        $myPosts[] = $post;
    }

    $cardsContent .= \Stratum\render('assets/components/pills', [
        'heading' => __('Filter by:', 'stratum'),
        'root' => [
            'name' => __('All', 'stratum'),
            'slug' => 'all',
            'url' => get_post_type_archive_link('rmg-work'),
            'selected' => is_post_type_archive('rmg-work')
        ],
        'items' => get_terms([
            'taxonomy' => 'rmg-work-category',
        ])
    ]);

    $cardsContent .= \Stratum\render('assets/components/selecturl', [
        'root' => [
            'name' => __('All', 'stratum'),
            'url' => get_post_type_archive_link('rmg-work'),
            'selected' => is_post_type_archive('rmg-work')
        ],
        'items' => get_terms([
            'taxonomy' => 'rmg-work-category',
        ])
    ]);

    // -------------------------------------------------------------------------
    // Add the posts to our content output
    // -------------------------------------------------------------------------
    $content .= \Stratum\render('assets/components/cards', [
        'cards' => $myPosts,
        'alternatecards' => true,
        'featured' => $showFeatured,
        'content' => $cardsContent,
    ]);

    // -------------------------------------------------------------------------
    // Output everything
    // -------------------------------------------------------------------------
    echo \Stratum\render('assets/components/main', $content);

    echo \Stratum\render('partials/wordpress/posts-pagination');
} else {
    echo \Stratum\render(
        'assets/components/main',
        \Stratum\render('partials/wordpress/content-none')
    );
}

get_footer();
